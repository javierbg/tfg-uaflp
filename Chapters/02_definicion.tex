\chapter{Definición del problema}

\label{cap:definicion}

\section{Problema real}

En esencia, el problema a resolver consiste en ubicar una serie de departamentos dentro de una planta de trabajo. Más concretamente, se busca obtener soluciones que sean buenas pero a la vez variadas, para que un experto las estudie y compare posteriormente.

El \ac{FLP} se presenta en numerosas formas. En este proyecto se abordará la resolución de problemas del tipo \ac{UA-FLP}, en español <<Problema de distribución en planta de área desigual>>.

Para la codificación de las soluciones se utilizará la representación conocida como \ac{FBS}, la cual se explica en más detalle en el capítulo \ref{cap:antecedentes}.

Los objetivos a alcanzar en este problema son:

\begin{itemize}
	\item Asignar a cada departamento el \myemph{área necesaria} ($A_{i}$ para el departamento $i$) sin sobrepasar el área disponible (siendo $H$ el alto del área disponible y $W$ el ancho).

	\begin{equation}
	\sum_{i=1}^{n} A_{i} \leq H \times W
	\end{equation}
	\item Mantener unos ciertos límites en la \myemph{relación de aspecto} de las dimensiones del departamento.

	\begin{equation}
	\alpha_{i} = \frac{\max (H_{i}, W_{i})}{\min (H_{i}, W_{i})} \leq \alpha_{max} \quad \forall i \in \{1,\dots,n\}
	\end{equation}
	Donde $H_{i}$ y $W_{i}$ son el alto y el ancho asignados respectivamente al departamento $i$.

	\item Minimizar el \myemph{flujo de materiales} entre departamentos lejanos.

	\begin{equation}
	\min \thickspace OF = \sum_{i=1}^{n} \left ( \sum_{j=1, j \neq i}^{n} f_{ij} d_{ij} \right )
	\end{equation}
	Siendo $f_{ij}$ el flujo entre los departamentos $i$ y $j$ y $d_{ij} = d_{ji}$ la distancia entre los mismos.
\end{itemize}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/facility_layout.png}
	\caption[Aplicación industrial del FLP]{En el \ac{FLP} se busca optimizar el flujo de materiales en una planta, en ocasiones de índole industrial}
\end{figure}

\section{Problema técnico}

Para la definición del problema técnico se seguirá la técnica \ac{PDS} que consta de los siguientes puntos clave.

\subsection{Funcionamiento}

\begin{itemize}
	\item El programa implementará un Algoritmo Evolutivo encargado de obtener soluciones apropiadas para el problema de distribución en planta.

	\item El algoritmo tendrá como resultado varias soluciones distintas.

	\item Se priorizará la diversidad de las soluciones obtenidas.
\end{itemize}

\subsection{Entorno}

\begin{itemize}
	\item Entorno \myemph{software}: Se hará uso de lenguajes, librerías y módulos de software libre que faciliten el desarrollo del programa. Todos ellos deberían ser fácilmente instalables utilizando un gestor de paquetes. % Se utilizará el lenguaje de programación \emph{Python 3}, el cual cumple ampliamente estos puntos.

	\item Entorno \myemph{hardware}: El programa se podrá ejecutar en cualquier dispositivo en el que estén disponibles los elementos mencionados anteriormente. Sin embargo, el entorno óptimo de ejecución del programa será un sistema multinúcleo, que permita hilos de ejecución paralelos.

	\item Entorno de \myemph{usuario}: El programa tendrá un funcionamiento relativamente sencillo, enfocado a ser usado por personal de investigación.
\end{itemize}

\subsection{Vida esperada}

Al tratarse de un proyecto de investigación, la vida esperada del programa es incierta. Ésta se verá sometida a los distintos avances que se hagan en el campo del \ac{FLP}.

\subsection{Ciclo de mantenimiento}

Si bien el programa se desarrollará con la facilidad de extensión en mente éste se considerará cerrado con la entrega del Trabajo de Fin de Grado. Gracias a la modularidad que ofrecen los \acp{AG}, el programa se prestará a modificaciones y futuras mejoras en otros entornos de investigación.

\subsection{Competencia}

En la literatura se pueden encontrar numerosas propuestas de algoritmos que tratan de obtener soluciones óptimas (aspirando al \emph{óptimo global}) para problemas de tipo \ac{FLP} y \ac{UA-FLP}, muchos de ellos basándose también en la representación \ac{FBS} \cite{PalomoRomero2017151} \cite{KULTURELKONAK2012614} \cite{KULTURELKONAK2011B} \cite{WONG20105523} \cite{KONAK2006660}.

Así mismo, se encuentran técnicas de mantenimiento de la diversidad en \aclp{AG}, aunque aplicado a otros problemas.

Sin embargo, no se han podido encontrar propuestas que tengan los objetivos de obtención de soluciones diversas aplicado al \ac{UA-FLP} como la abordada en este proyecto.

\subsection{Aspecto externo}

El programa se controlará a través de una \ac{CLI}. Tanto el código como los mensajes de ayuda y documentación se desarrollarán en inglés.

\subsection{Estandarización}

El código se construirá siguiendo algunos de los principios del \ac{PEP} número 8 \cite{pep8}. Entre otras cosas, esto consiste en lo siguiente:

\begin{itemize}
	\item La codificación de todos los ficheros de código se realizará en UTF-8. Adicionalmente a esto, el resto de ficheros de texto utilizados, aún no siendo código fuente, también utilizarán esta codificación.

	\item Se utilizará una indentación de 4 espacios.

	\item Tan sólo se importará un módulo por línea.

	\item Los módulos se importarán en el siguiente orden: primero módulos de la librería estándar, a continuación módulos de librerías de terceros y finalmente módulos locales.

	\item Se utilizarán identificadores con nombres representativos para facilitar la comprensión y documentación. Seguirán las siguientes normas:
	\begin{itemize}
		\item Los módulos se nombrarán en minúscula y, si fuera preciso, con guión bajo para mejorar la legibilidad.
		\item Las clases y <<tipos variable>> se nombrarán con el estilo \foreign{CamelCase}.
		\item Las funciones y variables se nombrarán en minúscula y con guiones bajos como espaciado.
	\end{itemize}
\end{itemize}

El formato de los datos de entrada del programa (problema, parámetros...) así como los de salida (soluciones) seguirá el mismo formato que aquel utilizado por la librería de \ac{FLP} ya disponible.

\subsection{Calidad y fiabilidad}

El programa será testado para comprobar su correcto funcionamiento, tanto con revisiones manuales de código como con tests automáticos.

\subsection{Programación de tareas}

Se realizarán las siguientes tareas, en un principio de forma secuencial pero recurriendo a un esquema iterativo cuando fuera necesario.

\subsubsection*{Estudio}

Previamente a la ejecución del proyecto se estudiarán en profundidad tanto el problema a tratar como las potenciales herramientas a utilizar. Más concretamente, esto abarca:

\begin{itemize}
	\item El \myemph{problema de distribución en planta}, sus variantes (\ac{UA-FLP}) así como la representación a utilizar (\ac{FBS}). Se recurrirá en este caso a previas publicaciones sobre el tema en la literatura.

	\item El \myemph{lenguaje de programación} para la implementación, haciendo especial hincapié a los módulos enfocados al desarrollo de \acp{AG} y al multiprocesamiento.
\end{itemize}

\subsubsection*{Diseño y codificación}

Durante esta fase se ideará conceptualmente el algoritmo: partes, estrategias a utilizar, datos a obtener, etc.

Una vez decidido esto se diseñará una arquitectura para la organización del mismo.

\subsubsection*{Implementación}

Haciendo uso de los conocimientos y herramientas estudiados, se implementará el código del programa: entrada/salida, codificación del algoritmo, comunicación entre procesos, etc.

\subsubsection*{Pruebas}

Antes de proseguir con el proyecto se realizarán pruebas para comprobar su correcto funcionamiento. Se diseñará un comportamiento deseado haciendo uso de parámetros de entrada controlados.

\subsubsection*{Experimentación}

Para comprobar el potencial y la eficacia del algoritmo diseñado se realizarán experimentos con distintos parámetros realizando medidas de eficacia del mismo (\foreign{fitness} y diversidad). Los resultados obtenidos se compararán con aquellos publicados con anterioridad cuando fuera posible.

\subsubsection*{Documentación}

Finalmente se plasmará por escrito todo el recorrido del proyecto, desde su concepción, decisiones y conclusiones obtenidas. Se incluirá la totalidad del código fuente documentado y comentado.

\subsection{Pruebas}

Se realizarán dos tipos de pruebas:

\begin{itemize}
	\item Pruebas de funcionamiento: estas pruebas se realizarán durante la etapa de codificación y su objetivo será evitar fallos en la implementación del programa. Se realizarán tanto pruebas de caja blanca o estructurales como pruebas de caja negra, a nivel de componentes y de sistema.

	\item Pruebas de eficiencia: Se realizarán diversos experimentos utilizando resultados de referencia en la literatura para medir la eficacia del algoritmo según los objetivos definidos. Para ello se definirán primero estos experimentos y a continuación se analizarán en busca de resultados relevantes.
\end{itemize}

\subsection{Seguridad}

Los datos manejados por el programa no son en ningún caso sensibles o de carácter privado, así que no existe la necesidad de protección contra terceros.
