\chapter{Antecedentes}

\label{cap:antecedentes} % Change X to a consecutive number; for referencing this chapter elsewhere, use \ref{ChapterX}

\section{Algoritmos Genéticos}

Los \acfp{AG} son un tipo de metaheurísticas englobadas dentro de la categoría de Algoritmos Evolutivos. Estos se basan en ideas de la selección y evolución natural para generar soluciones manteniendo un equilibrio excelente entre diversificación (probar soluciones dispares) e intensificación (concentrarse en soluciones aparentemente buenas).

Los Algoritmos Evolutivos generan soluciones mediante un proceso de mutación a lo largo de distintas generaciones. Sobre esto, los Algoritmos Genéticos introducen el concepto de población de individuos (soluciones) y cruce entre los mismos. Su funcionamiento se detalla a continuación.

\subsection{Funcionamiento de los Algoritmos Genéticos}

El principal componente de un Algoritmo Genético es la \myemph{población} de individuos (cada uno de ellos una solución al problema), inicializados de forma aleatoria al comienzo del algoritmo.

Como cualquier otra metaheurística, es necesaria una \myemph{función objetivo} o \foreign{fitness} a optimizar. Esta debe estar directamente relacionada con la calidad de cada uno de los individuos/soluciones. El \ac{AG} hará uso de esta medida para introducir una \myemph{presión selectiva} sobre los miembros de la población, es decir, los más aptos (mayor \foreign{fitness}) tendrán una mayor probabilidad de producir descendencia.

Lo que diferencia a un \ac{AG} de otros Algoritmos Evolutivos es el operador de \myemph{cruce}. Este es el encargado de combinar el código genético de varios individuos para generar otros nuevos, tratando de mantener la información de las soluciones <<padre>>. La elección de este operador es crucial en la efectividad del algoritmo, ya que es necesario que se cumpla la hipótesis de que, en general, generará individuos con características combinadas de los individuos seleccionados.

En cada iteración del algoritmo se evalúan todos los miembros de la población mediante la función objetivo. En base a esta, se seleccionan individuos de la población para generar otros nuevos mediante el operador de cruce. Existe la opción de introducir \myemph{mutaciones} en las soluciones generadas para ampliar la diversidad de la población. Los individuos generados (puede que mezclados con algunos de los individuos de la población anterior) pasarán a ser la siguiente población y el proceso se repite hasta que se cumpla la condición de parada elegida (tiempo computacional o número de iteraciones máximos).

Los \acp{AG} son altamente modulares debido a lo intercambiables que son sus diferentes operadores (de cruce, de selección, de mutación, etc.). Existen numerosas \myemph{variantes} en base a estas elecciones, pero su funcionamiento general permanece como el anteriormente mencionado.

\subsection{El modelo de islas}

En ocasiones, los óptimos locales pueden llegar a ser un problema debido a la llamada convergencia prematura, por lo que es necesario la utilización de métodos que permitan aumentar la diversidad de la población, haciendo más difícil caer en la situación anteriormente señalada.

Según este modelo, se mantienen no solo una sino varias poblaciones distintas (denominadas \myemph{islas}) que evolucionan por separado. Cada cierto número de iteraciones se produce un proceso de \myemph{migración} entre las distintas islas. La forma en la que estas migraciones se realizan está determinada por la \myemph{topología} de las islas. Entre las más comunes están las topologías en anillo, maestro-esclavo y la distribución espacial granulada \cite{Cantu-Paz98asurvey}.

Este modelo permite la exploración de distintas partes del espacio de búsqueda en cada población, evitando así la convergencia temprana con la posibilidad de ofrecer soluciones genéticamente diversas.

\subsubsection*{Islas localizadas}

Una interesante adición al modelo de islas, donde lo único que diferencia a una isla de otra es su población inicial, es el modelo de \emph{islas localizadas} o \emph{estrategia de localización} \cite{Gozali2017}.

Este modelo toma como referencia la variedad de hábitats del mundo real y consiste en ejecutar \myemph{un \ac{AG} distinto en cada una de las islas} (\pej cambiando los parámetros en cada una de ellas), consiguiendo así potenciar distintas características de los individuos en cada una de ellas. Esto consigue, en esencia, aumentar la diversidad de las soluciones exploradas.

\subsection{Paralelismo en Algoritmos Genéticos}

La principal \myemph{carga computacional} en los \acp{AG} recae sobre el \myemph{proceso de evaluación} de soluciones. Por suerte, este cálculo es independiente entre los individuos de la población, por lo que existe un gran potencial de paralelización en este campo.

Adicionalmente, el modelo en islas separa aún más la interdependencia entre cada población, por lo que es posible realizar el cálculo completo de cada población (no sólo la evaluación) en paralelo hasta que llegue el momento de la migración.

Por último, el modelo de islas localizadas, donde cada una de las islas puede tomar un tiempo muy distinto de ejecución debido a su diversidad paramétrica, se presta a una arquitectura \foreign{software} mucho menos interdependiente. Es muy apropiado para esta situación una comunicación \myemph{asíncrona} entre procesos. Evitando la necesidad de sincronizar el proceso de evolución de las islas se consigue que uno de ellos no retrase a los demás y se aprovechen mejor los recursos disponibles.

\section{Estructura de Bahías Flexibles}

Una de las estructuras para las soluciones más utilizada en la literatura es la \acf{FBS}, o <<Estructura de Bahías Flexibles>> en español. Ésta consiste en la disposición de los departamentos en franjas o \emph{bahías} de un mismo ancho, distribuyendo el alto de cada departamento de forma acorde a su área necesaria. El problema se reduce entonces a la decisión de en qué orden colocar los departamentos y el número de departamentos en cada columna \cite{tate1995unequal}.

Esta estructura no es sólo sencilla de codificar sino que también presenta soluciones más fácilmente traducibles a en un entorno real. Puede verse un ejemplo de la representación \ac{FBS} en la figura \ref{fig:fbs}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{/ejemplo_fbs.png}
	\caption{Representación gráfica de una solución de tipo FBS}
	\label{fig:fbs}
\end{figure}

\section{Librerías}

Debido a la gran cantidad de documentación y a lo extendido que se encuentra entre la comunidad científica, el desarrollo se realizará en el lenguaje de programación Python. Existen numerosas herramientas tanto dentro de la librería estándar como de terceros para el desarrollo de \acp{AG} así como para otros de los objetivos del proyecto:

\begin{itemize}
	\item \module{inspyred} \cite{inspyred}, un \foreign{framework} de desarrollo para la implementación de algoritmos bio-inspirados incluyendo \acp{AG}, Algoritmos Coevolutivos y \foreign{swarm intelligence} (inteligencia de enjambre).
	\item DEAP (\foreign{Distributed Evolutionary Algorithms in Python}) \cite{DEAP_JMLR2012} es una librería enfocada a facilitar el desarrollo de esta clase de algoritmos, permitiendo utilizar todos los elementos nativos de Python para desarrollar representaciones así como generalizar estrategias de evolución, evaluación de soluciones, \foreign{benchmarks} y mucho más.
	\item El módulo \module{multiprocessing} de la librería estándar, que facilita la abstracción e implementación de cálculos en paralelo. DEAP facilita el uso de este módulo para paralelizar sus cálculos.
	\item SCOOP (\foreign{Scalable COncurrent Operations in Python}) \cite{SCOOP_XSEDE2014} es una herramienta de paralelización válida tanto para multiprocesamiento local como para \foreign{clusters} de computadores. También está soportada por DEAP.
\end{itemize}

\section{Proyectos}

Existe una gran cantidad de trabajo previo realizado en el grupo de trabajo de Proyectos de Ingeniería. Algunos Proyectos de Fin de Carrera dedicados a \acp{AG} aplicados al \ac{FLP} son:

\begin{itemize}
	\item La implementación y comparativa de \acp{AG} que utilizan \emph{curvas de llenado del espacio} \cite{pfc2009}.
	\item La comparativa de resultados de \acp{AG} que utilizan la \emph{representación \ac{FBS}} \cite{pfc2010-1}.
	\item El estudio del \emph{involucramiento del usuario} en el proceso de búsqueda \cite{pfc2010-2}.
	\item La aplicación de \emph{sistemas distribuidos} a esta clase de algoritmos \cite{pfc2011-1}.
	\item La visualización de las soluciones a este problema \cite{pfc2011-2}.
	\item La estandarización y modularización de estos algoritmos para crear un marco de trabajo común \cite{pfc2012}.
	\item El uso de un \emph{algoritmo coevolutivo} para mejorar las soluciones \cite{GataMoreno}.
\end{itemize}