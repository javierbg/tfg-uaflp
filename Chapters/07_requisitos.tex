\chapter{Especificación de requisitos}
\label{cap:especificacion} 

En este capítulo se describirán aquellos requisitos que se exigen del sistema a desarrollar.

\section{Componentes del algoritmo}

A continuación se especifican todos aquellos elementos que conforman el algoritmo.

\subsection{Individuos} \label{sec:individuos}

Los individuos son las unidades que constituyen cada una de las poblaciones utilizadas en el algoritmo genético. Cada uno de ellos se corresponde con una posible solución a la instancia del problema.

Las soluciones a explorar tendrán la estructura de de bahías flexibles, como ya se ha explicado en el capítulo \ref{cap:antecedentes}. Esta estructura permite una codificación muy sencilla, fácilmente utilizable en conjunción al resto de elementos del \ac{AG}. Esta codificación consta de dos partes:

\begin{itemize}
	\item Una \myemph{permutación} de los departamentos que es necesario acomodar, esto es, una secuencia ordenada en la que cada departamento aparece una única vez. Ésta determinará el orden en el que se posicionarán los departamentos, de izquierda a derecha y de arriba a abajo (considerando la dimensión mayor de la planta como eje horizontal). Por ejemplo, en un problema con 8 departamentos identificados con los números del 1 al 8, una posible permutación sería: $\langle 4, 7, 2, 6, 8, 1, 3, 5 \rangle$.
	
	\item Una secuencia de valores booleanos que describe los \myemph{puntos de corte} en los que se pasa de una bahía a la siguiente en la permutación anterior. Un valor negativo o $0$ significa que el siguiente departamento permanecerá en la misma bahía, mientras que uno positivo o $1$ que la bahía actual se cerrará y se comenzará una nueva. El tamaño de esta secuencia será una unidad menor que el número de departamentos (al no tener el último departamento un siguiente). Según el ejemplo anterior de un problema con 8 departamentos, una secuencia válida podría ser: $\langle 0, 1, 0, 0, 0, 1, 0 \rangle$.
\end{itemize}

Una representación de los valores de ejemplo anteriores puede observarse en la figura \ref{fig:ejemplo-individuo}.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/ejemplo_individuo.pdf}
	\caption[Ejemplo de individuo para el problema \problem{O8}]{Ejemplo de individuo para el problema \problem{O8}. \\ Permutación: $\langle 4, 7, 2, 6, 8, 1, 3, 5 \rangle$, Cortes: $\langle 0, 1, 0, 0, 0, 1, 0 \rangle$}
	\label{fig:ejemplo-individuo}
\end{figure}

\subsection{Función objetivo}

Como se menciona en el capítulo \ref{cap:definicion}, el objetivo buscado en el \ac{FLP} es reducir la distancia entre departamentos con mayor flujo de materiales entre ellos. Expresado matemáticamente, esto se traduce a:

\begin{equation} \label{eq:fitness-material-flow}
\min \thickspace OF = \sum_{i=1}^{n} \left ( \sum_{j=1, j \neq i}^{n} f_{ij} d_{ij} \right )
\end{equation}
Donde $OF$ es la función objetivo, $f_{ij}$ es el flujo entre los departamentos $i$ y $j$ y $d_{ij} = d_{ji}$ es la distancia entre los mismos. El flujo $f_{ij}$ es parte de la definición de la instancia del problema, así que sólo queda la cuestión de la distancia.

La distancia considerada en este caso será la \emph{distancia entre centroides}, es decir, la distancia entre los puntos medios de los departamentos (donde sus diagonales se cruzan). Éste es el punto más característico de la posición del departamento.

Esta distancia puede considerarse como distancia \emph{euclidiana}, distancia \emph{de Manhattan} o distancia \emph{de Chebyshev}. De nuevo, el tipo de distancia es parte de la definición de la instancia del problema. Una ilustración de los centroides y estos tres tipos de distancia puede observarse en la figura \ref{fig:distancias}.

\begin{figure}
	\begin{tabular}{ccc}
		\includegraphics[width=0.33\textwidth]{Figures/dist_euclidiana.eps} &  
		\includegraphics[width=0.33\textwidth]{Figures/dist_manhattan.eps} &  \includegraphics[width=0.33\textwidth]{Figures/dist_chebyshev.eps} \\
		(a) Distancia euclidiana & (b) Distancia de Manhattan & (c) Distancia de Chebyshev
	\end{tabular}
	\caption[Ilustración de los centroides de los departamentos y los tres tipos de distancias]{Ilustración de los centroides de los departamentos y los tres tipos de distancias. El punto de intersección de la X roja se corresponde con el centroide y la longitud de la flecha azul con el valor de la misma.}
	\label{fig:distancias}
\end{figure}

\subsubsection*{Penalización dinámica}

Existe sin embargo un problema que todavía no se ha abordado en la evaluación de soluciones: la representación \ac{FBS} no asegura que todas las soluciones sean válidas.

Como ya se ha mencionado en la definición del problema del capítulo \ref{cap:definicion}, el problema tiene la restricción de mantener ciertos límites en cuanto a la relación de aspecto de los lados de cada departamento para que éstos tengan dimensiones realistas. Descrito matemáticamente, se busca mantener la siguiente afirmación:

\begin{equation}
\alpha_{i} = \frac{\max (H_{i}, W_{i})}{\min (H_{i}, W_{i})} \leq {\alpha_{max}}_i \quad \forall i \in \{1,\dots,n\}
\end{equation}
Donde $H_{i}$ y $W_{i}$ son el alto y el ancho respectivamente asignados al departamento $i$, $\alpha_{i}$ es por tanto su ratio de aspecto y ${\alpha_{max}}_i$ el ratio de aspecto máximo permitido para ese mismo departamento.

Simplemente \emph{desechar estas soluciones} durante el proceso de búsqueda es una mala elección, ya que se \myemph{disminuye la diversidad} de la población y se corre un \myemph{mayor riesgo de caer en óptimos locales} cuando todavía existe potencial de mejora. Por encima de todo esto, generar individuos aleatorios válidos para la población inicial del algoritmo se hace imposible conforme ${\alpha_{max}}_i$ disminuye (para valores de ${\alpha_{max}}_i \leq 4$ la proporción de individuos válidos generados aleatoriamente se acerca al $0.005\%$ \cite{tate1995unequal}).

Aún mejor que ésto es \emph{mantener estas soluciones}, aunque \emph{penalizando su valor} de \foreign{fitness} en una medida proporcional a cuánto incumplen esta restricción. De esta forma las soluciones inválidas \myemph{pueden formar parte del proceso de búsqueda} y el algoritmo, guiado por la función objetivo, tenderá a generar soluciones válidas (las cuales no tienen penalización alguna).

Ya existen en la literatura propuestas para esta penalización. En el algoritmo implementado se hará uso de una penalización propuesta por Tate y Smith en 1995 \cite{tate1995unequal}, la cual considera \emph{más importante el número de departamentos inválidos} sobre el grado de invalidez de cada uno de ellos. Su formulación matemática es la siguiente:

\begin{equation}
P = (D_{inv})^k (V_{val} - V_{todo})
\end{equation}
Donde $D_{inv}$ es el número de departamentos con un ratio de aspecto inválido, $V_{val}$ es el mejor valor de la función objetivo $OF$ encontrado para soluciones válidas, $V_{todo}$ es lo mismo aplicado a todas las soluciones y el exponente $k$ controla la severidad de la penalización. En el algoritmo diseñado se ha considerado $k = 1$.

De esta forma el algoritmo optimizará las soluciones utilizando la siguiente función de \foreign{fitness} $F$:
\begin{equation} \label{eq:fitness}
	\min \thickspace F = OF + P = \sum_{i=1}^{n} \left ( \sum_{j=1, j \neq i}^{n} f_{ij} d_{ij} \right ) + (D_{inv})^k (V_{val} - V_{todo})
\end{equation}
	
Esta penalización se considera \emph{dinámica}, ya que su valor depende del historial de ejecución del algoritmo. Ésto podría suponer un impedimento sobre la paralelización más granulada del algoritmo (es decir, a nivel de evaluación), ya que no se podrían evaluar dos soluciones concurrentemente, pero se explicará más adelante cómo se evita en este algoritmo. Nótese además como para soluciones válidas se cumple que $D_{inv} = 0$ y por tanto $P = 0$ (no hay penalización).

Es importante indicar que el algoritmo \emph{debe evitar dar soluciones finales con departamentos inválidos} ya que éstas no se consideran soluciones al problema. Estas soluciones inválidas sólo se utilizarán para guiar el proceso de búsqueda.
	
\subsection{Operadores genéticos} \label{subsec:operadores-geneticos}

El funcionamiento de un \ac{AG} puede descomponerse en distintos operadores, altamente modulares, que pueden ser utilizados por algoritmos muy variados. Cada uno de ellos tiene un objetivo concreto así como distintas opciones entre las que elegir.

A continuación se detallarán todos los operadores genéticos del algoritmo.

\subsubsection*{Operador de inicialización}

El operador de inicialización se encarga de crear las poblaciones iniciales del \ac{AG}.

Dado un tamaño de la población $M$, es necesario generar $M$ variaciones distintas de los componentes del individuo, esto es, su secuencia de permutaciones y su vector de cortes, como se explica en la sección \ref{sec:individuos}.

\begin{itemize}
	\item Para crear una \myemph{permutación} se genera una secuencia ordenada de la misma $\langle 1, 2, 3, \dots \rangle$ y se desordena según el algoritmo de Fisher-Yates \cite{Knuth:1981:ACP:270146}.
	
	\item Para crear el \myemph{vector de cortes} se elegirá para cada posición del mismo un 1 o un 0 de forma aleatoria con misma probabilidad (50/50).
\end{itemize}

\subsubsection*{Operador de selección}

El operador de selección es utilizado antes del resto y se encarga de decidir qué individuos permanecen en la población tras realizar la evaluación de los mismos. Utiliza la información de \foreign{fitness} para introducir \foreign{presión selectiva} en la población, dando mayores probabilidades de permanecer a aquellos individuos más aptos.

El algoritmo implementa una selección por \myemph{torneo binario}. Éste es un operador de selección muy utilizado en la literatura que consiste en lo siguiente.

Hasta seleccionar todos los individuos de la población:
\begin{itemize}
	\item \emph{Seleccionar dos individuos de la población aleatoriamente} según una distribución uniforme. Estas dos selecciones son \emph{independientes}, por lo que se puede seleccionar el mismo individuo dos veces.
	\item El \emph{individuo con un mejor \foreign{fitness}} de los dos se añade a los individuos seleccionados. En caso de empate, coger el primero.
\end{itemize}

Nótese como cuando se selecciona un individuo éste no se extrae de la población, por lo que \emph{algunos individuos pueden ser seleccionados varias veces} y \emph{otros pueden no ser seleccionados nunca}.

\begin{figure}
	\centering
	\includegraphics[width=0.3\textwidth]{Figures/torneo_binario.pdf}
	\caption[Torneo binario]{Ilustración del funcionamiento del torneo binario. Se especifica a continuación de cada individuo $S_i$ su \foreign{fitness}}
\end{figure}

Este operador de selección permite crear presión selectiva pero a la vez deja una (menor) oportunidad a los individuos menos aptos para continuar en la población, pudiendo aprovechar su código genético para así \emph{explorar de forma más exhaustiva el espacio de búsqueda}.

\subsubsection*{Operador de cruce}

El operador de cruce determina cómo los individuos de cada población se combinan para obtener nuevas soluciones que combinen las características de las originales.

\begin{itemize}
	\item En primer lugar se agrupan todos los individuos de la población en pares: primero con segundo, tercero con cuarto, etc.
	\item A continuación, para cada par, se realiza el cruce con una \myemph{probabilidad parametrizada}.
	\item En caso afirmativo, el cruce se realiza \foreign{in-place}, sustituyendo a los padres originales por su descendencia (otros dos nuevos individuos).
\end{itemize}

Es necesario cruzar, para cada par de individuos, tanto su permutación de departamentos como su vector de cortes, tal y como se describe en la sección \ref{sec:individuos}.

El cruce de la \myemph{permutación de departamentos} es del tipo \ac{PMX}, el cual consigue mantener la integridad de la permutación. Consiste en lo siguiente:
\begin{itemize}
	\item Se eligen \emph{dos puntos aleatorios de la secuencia} (distribución uniforme), delimitando \emph{dos subsecuencias}, una en cada individuo.
	\item En cada uno de los individuos, \emph{intercambiar iterativamente} cada uno de los elementos de la subsecuencia por el resto de forma que coincidan con los de la subsecuencia original del otro individuo.
\end{itemize}

Una ilustración del \ac{PMX} puede observarse en la figura \ref{fig:op-cruce}a.

\begin{figure}
	\centering
	\begin{tabular}{CC}
		\includegraphics[width=0.3\textwidth]{Figures/pmx.pdf} &  
		\includegraphics[width=0.3\textwidth]{Figures/spx.pdf} \\
		(a) Cruce PMX & (b) Cruce SPX
	\end{tabular}
	\caption{Ejemplos de operadores de cruce.}
	\label{fig:op-cruce}
\end{figure}

Por otro lado, el cruce del \myemph{vector de cortes} es del tipo \ac{SPX}. El proceso consta de lo siguiente:
\begin{itemize}
	\item Se elige \emph{un punto de corte aleatorio} del vector (distribución uniforme) dividiendo cada uno de los vectores en dos partes.
	
	\item Se generan dos individuos: uno con la primera parte del primer padre y la segunda parte del segundo padre y viceversa.
\end{itemize}

De igual forma, se puede observar una ilustración del \ac{SPX} en la figura \ref{fig:op-cruce}b.

\subsubsection*{Operador de mutación}

El operador de mutación actúa después del operador de cruce y su propósito es \emph{alterar ligeramente la población} para \emph{evitar su estancamiento genético}.

Para que éste sea efectivo, la mutación debe aplicarse a una porción pequeña de la población. Ésto se consigue aplicándola con una pequeña probabilidad (entre un \SI{1}{\percent} y un \SI{5}{\percent}) a cada individuo tras efectuar el cruce. Una probabilidad de mutación \emph{demasiado grande} provocaría el equivalente a una \emph{búsqueda aleatoria}, mientras que una \emph{muy pequeña} (o la ausencia de mutación) \emph{limitaría el espacio de búsqueda según la población inicial}.

Esta mutación puede aplicarse a los dos aspectos de un individuo: a su permutación de departamentos o a su vector de cortes.

Aplicado a la \myemph{permutación de departamentos} la mutación consiste en \emph{intercambiar dos valores aleatorios de la secuencia}.

Si se aplica al \myemph{vector de cortes}, existen dos opciones: o bien \emph{se invierte una posición aleatoria del vector} o bien \emph{se desplaza uno de los $1$s del mismo} a izquierda o derecha. La primera mutación permite \emph{introducir o quitar cortes} de la solución original, mientras que la segunda mantiene la cantidad pero \emph{cambia los departamentos afectados por un corte}.

\subsection{Medidas de diversidad}

Como se explicará a continuación, uno de los principales objetivos del algoritmo desarrollado es mantener la diversidad de la población para mejorar la diversidad de las soluciones obtenidas. Para conseguir tener éxito en el cumplimiento de este objetivo es necesario primero definir rigurosamente qué se entiende por diversidad.

La \myemph{diversidad} de una población la constituye la \emph{variedad de características} que presentan sus individuos. Por tanto, conviene definir en primera instancia cómo medir las diferencias entre dos individuos.

\subsubsection*{Distancia entre individuos}

El código genético de un individuo (su \emph{genotipo}) define de forma inequívoca como se manifestarán sus características observables (su \emph{fenotipo}), pero son las últimas las más apropiadas para medir las diferencias entre dos individuos ya que nuestro interés reside en la ejecución final de los mismos (es decir, las soluciones que representan).

En el caso del \ac{FLP} y las soluciones del tipo \ac{FBS}, un \emph{elemento resumen} bastante efectivo de las mismas es la \emph{posición de los centroides de los departamentos}.

Si, dadas dos soluciones distintas, consideramos $c_i = (x_i, y_i)$ la posición del centroide del departamento $i$ de la solución $S$ en coordenadas cartesianas y ${c_i}' = ({x_i}', {y_i}')$ su homónimo de la solución $S'$, entonces podemos definir la distancia entre dos individuos como:
\begin{equation}
	d_{indv}(S,S') = \sum_{i=1}^{N} d(c_i, {c_i}') = \sum_{i=1}^{N} \sqrt{(x_i - {x_i}')^2 + (y_i - {y_i}')^2}
\end{equation}
donde $N$ es el número de departamentos.

\begin{figure}[]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/dist_individuos.pdf}
	\caption{Distancia de centroides de un departamento entre dos soluciones distintas}
	\label{fig:dist-individuos}
\end{figure}

Sin embargo, hay una \emph{importante consideración} a tener en cuenta: una solución $S$ es exactamente igual de válida a su versión rotada \SI{180}{\degree}, denotada por $S^*$. Si no se tiene esto en cuenta, el algoritmo podría dar soluciones que sean \emph{supuestamente diversas} pero en realidad sean muy parecidas en sus versiones rotadas.

Para solventar esta problemática, se propone lo siguiente: tomar como distancia real el mínimo de la distancia a la versión rotada y la versión sin rotar. Descrito matemáticamente:
\begin{equation}
	d_{real}(S,S') = \min \left ( d_{indv}(S,S'), d_{indv}(S^*,S') \right )
\end{equation}

\subsubsection*{Diversidad de una población}

Construyendo encima del concepto de distancia es posible definir una medida de diversidad en una población concreta.

Para abordar el problema de forma plena sería necesario tener en cuenta cada una de las distancias entre pares de soluciones. En una población de $M$ individuos, esto supondría un total según las combinaciones de $M$ elementos tomados de $2$ en $2$:
\begin{equation}
	C_{M,2} = \binom{M}{2} = \frac{M!}{2!(M-2)!} = \frac{M(M-1)}{2} \in O(M^2)
\end{equation}
valor que crece de forma \emph{cuadrática}. Esto supone una combinatoria insostenible, así que se hace necesario una relajación de esta medida.

En su lugar puede utilizarse un único \emph{individuo de referencia} con el que comparar el resto. Por razones que se detallan más adelante, una buena elección para este individuo es aquél con mejor \foreign{fitness} (el más apto de la población). Así, definimos una población $B = \set{S^1, S^2, \dots, S^M}$ tal que $S^1$ es el individuo dominante, es decir:
\begin{equation}
	F(S^1) \geq F(S^j)  \quad \forall j \in \set{2, 3, \dots, M}
\end{equation}
donde $F$ es la función de \foreign{fitness} definida en la ecuación \ref{eq:fitness}.

Podemos definir la diversidad de $B$ como la \emph{distancia mediana} del individuo dominante al resto de individuos $D'(B)$. En lenguaje matemático:
\begin{gather}
	d_{real}(S^1, S^j) = \Delta_p \quad \forall j \in \set{2, 3, \dots, M} \\
	\Delta_p \leq \Delta_q \quad \forall p < q \\
	D'(B) =
	\begin{cases}
	\frac{\Delta_{\ceil{M/2}} + \Delta_{\floor{M/2}}}{2} & \text{si }M\text{ es impar} \\
	\Delta_{M/2} & \text{si }M\text{ es par}
	\end{cases}
\end{gather}

Esta ya es una medida válida de la diversidad de $B$, sin embargo, \emph{su escala es fuertemente dependiente} de las dimensiones del problema (esto es, alto y ancho de la planta y número de departamentos). Como se observará más adelante, se necesitará parametrizar un umbral de esta medida, así que sería conveniente \emph{hacerla independiente} de la instancia del problema a tratar.

Para ello se puede tomar una muestra de la escala del problema de la siguiente forma: se denotará por $d_{max}$ un valor de distancia <<máximo>> entre dos soluciones. Ya que la distancia máxima entre los centroides de dos departamentos es la diagonal de la planta y esto se suma tantas veces como departamentos haya, una forma de definir $d_{max}$ es la siguiente:
\begin{equation}
	d_{max} = N \cdot \sqrt{H^2 + W^2} 
\end{equation}

Y por tanto, la \emph{medida de diversidad independiente de la escala} $D(B)$ acaba siendo:
\begin{equation}
	D(B) = \frac{D'(B)}{d_{max}}
\end{equation}

\begin{figure}
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/ilustracion_diversidad.pdf}
	\caption[Diversidad de una población]{La diversidad de una población se calcula en base a la distancia entre su mejor individuo y el resto}
\end{figure}

\section{Estructura de los datos}

En el siguiente apartado se describirán los datos con los que trabajará el programa, tanto los que recibe como entrada como los que emite como salida.

\subsection{Datos de entrada} \label{subsec:datos-entrada}

\subsubsection*{Instancia del problema}

La instancia del problema consiste en los datos que definen un problema \ac{UA-FLP} concreto. Estos son:

\begin{itemize}
	\item Las dimensiones de la planta en cuestión, es decir, su ancho $W$ y su alto $H$.
	
	\item Una lista de los departamentos a ubicar, cada uno con su área mínima necesaria $A_i$, y su relación de aspecto máxima $\alpha_i$.
	
	\item La matriz de flujos de materiales entre departamentos $f_{ij}$.
\end{itemize}

\subsubsection*{Parámetros del algoritmo}

Los parámetros del algoritmo definen diferentes aspectos de su comportamiento. Éstos son los siguientes:

\begin{itemize}
	\item El \emph{número de islas}: $I$.
	\item El \emph{umbral de diversidad} a partir del cual se realiza la migración: $D_{min}$.
	\item El \emph{número de mejores individuos a mantener} para elegir las soluciones finales: $n_{keep}$.
	\item Los \emph{rangos de parámetros} de cada una de las islas. Al iniciar el algoritmo, se elegirá para cada isla $i$ un valor aleatorio dentro de este rango:
	\begin{itemize}
		\item El \emph{número de generaciones} total: $G_i$.
		\item El \emph{número de generaciones entre migraciones}: $C_i$.
		\item El \emph{tamaño de la población}: $M_i$.
		\item La \emph{probabilidad de mutación}: $P^{mut}_i$.
		\item La \emph{probabilidad de cruce}: $P^{cross}_i$.
	\end{itemize}
\end{itemize}

\subsection{Datos de salida} \label{subsec:datos-salida}

\subsubsection*{Soluciones}

El programa guardará posteriormente a su ejecución aquellos datos relativos a las soluciones encontradas y a valores relevantes de la misma ejecución.

\begin{itemize}
	\item Los \emph{parámetros concretos} elegidos para cada una de las islas.
	\item Una \emph{lista de las soluciones} obtenidas:
	\begin{itemize}
		\item Su \emph{genotipo}, esto es, su secuencia de departamentos y su vector de cortes.
		\item Su \emph{fenotipo}, es decir, las coordenadas de cada departamento en la planta.
		\item Su valor de \foreign{fitness} o aptitud.
	\end{itemize}
	\item \emph{Historial de migraciones} entre las islas durante la ejecución (cuántos individuos han migrado desde cada isla hasta cada isla).
	\item Datos relativos a la \emph{evolución de la diversidad} en las islas.
	\item \emph{Tiempo de ejecución}.
	\item \emph{Descripción del sistema} donde se ha ejecutado.
\end{itemize}

\section{Algoritmo de islas localizadas} \label{sec:islas-localizadas}

Finalmente, con todos los componentes y datos a tratar definidos, se describe a continuación el funcionamiento del algoritmo.

El \emph{algoritmo de islas localizadas} \cite{Gozali2017} añade sobre el esquema básico de los \acp{AG} los siguientes dos elementos cruciales:

\begin{itemize}
	\item Es un algoritmo \emph{en islas}, es decir, que existen varias poblaciones independientes que se comunican puntualmente mediante migraciones de individuos.
	
	\item Las islas se encuentran \emph{localizadas}. Basándose en la idea de la diversidad de hábitats naturales, cada una de las islas tendrá un entorno (parámetros) distinto que obliga a los individuos inmigrantes a adaptarse.
\end{itemize}

Estos dos añadidos tienen como meta la \emph{promoción de la diversidad}.

Para modelar este comportamiento, debido a la independencia de las islas entre sí y para aprovechar las capacidades de multiprocesamiento del \foreign{hardware}, se ha elegido una \emph{arquitectura gestor-trabajador} (también conocida como maestro-esclavo) en la que el gestor y cada uno de los trabajadores se ejecuta en un \emph{proceso distinto} y éstos \emph{se comunican de forma asíncrona}.

\begin{itemize}
	\item El papel del \myemph{gestor} será el de gestionar la \emph{comunicación} y el lanzamiento/finalización del resto de procesos. Ésto engloba la \emph{gestión de las migraciones}.
	
	\item Por su lado, cada uno de los \myemph{trabajadores} ejecutará el algoritmo de cada isla por separado, contactando con el gestor cuando fuera necesario.
\end{itemize}

Todos los individuos recibidos por el gestor (es decir, los mejores de cada isla tras cada ciclo generacional) son almacenados en un \emph{hall de la fama} que contendrá los $n_{keep} \cdot I$ mejores individuos recibidos. Al final del algoritmo se tomará la distancia mediana de cada uno de los individuos almacenados con el resto y el algoritmo tendrá como resultado los $I$ individuos con una mayor puntuación en este aspecto.

De esta forma se consigue que los resultados sean buenos (ya que se mantienen los mejores individuos recibidos) pero se promueve la diversidad (eligiendo aquellos más distintos al resto).