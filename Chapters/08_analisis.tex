\chapter{Análisis del sistema}
\label{cap:analisis}

Una vez definidos todos los requisitos del sistema se pasa a detallar cómo se conseguirá todo esto explicando el comportamiento del sistema.

\section{Generación aleatoria de individuos}

El algoritmo \ref{alg:inicializacion} genera un nuevo individuo aleatorio tal y como se especifica en la sección \ref{subsec:operadores-geneticos}. Se ha priorizado la aleatoriedad antes que una generación más guiada (por ejemplo con una heurística) para favorecer la exploración y evitar óptimos locales.

\begin{itemize}
	\item Para la permutación de departamentos se utiliza inicialmente una secuencia ordenada de enteros, desde $1$ hasta $n$, para luego desordenarla.
	\item En el caso de la secuencia de cortes, se crea inicialmente una secuencia de $n-1$ valores, todos ellos a $0$, y se decide con una probabilidad $50/50$ si cambiarlo a un $1$ o dejarlo como está. Se podría parametrizar la probabilidad de este proceso, pero el proceso de evolución corregirá rápidamente estas secuencias generadas y no tendrá un efecto importante.
\end{itemize}

\begin{algorithm}
	\caption{Crear un individuo aleatorio}
	\label{alg:inicializacion}
	\KwResult{nuevo individuo aleatorio}
	\KwData{$departamentos$, $cortes$, $temp$}
	$departamentos := \langle 1, 2, \dotsc , n \rangle$ \\
	$cortes := \langle 0, 0, \dotsc, 0 \rangle$ \tcp{$n-1$ veces} 
	\tcp{Desordenar $departamentos$}
	\ForEach{$k$ desde $n-1$ hasta $1$}{
		$cambio := aleatorio(\set{0, 1, \dotsc, k})$ \tcp{Distribución uniforme}
		$temp := departamentos[cambio]$ \\
		$departamentos[cambio] := departamentos[k]$ \\
		$departamentos[k] := temp$
	}
	\tcp{Aleatorizar $cortes$}
	\ForEach{$k$ desde $0$ hasta $n-2$}{
		\tcp{$rand$ devuelve un valor aleatorio en el rango $[0,1)$ (distribución uniforme)}
		\If{$rand() < 0.5$}{
			$cortes[k] := 1$
		}
	}
	\Return{$(departamentos, cortes)$}
\end{algorithm}

\section{Selección por torneo binario}

El algoritmo \ref{alg:torneo-binario} selecciona los nuevos individuos de la población tal y como se describe en la sección \ref{subsec:operadores-geneticos}.

Se comienza con una población $B'$ vacía ($\emptyset$) a la que se van añadiendo individuos. En cada iteración se toman dos individuos de la población original $B$ mediante un muestreo aleatorio con reemplazo, es decir, se puede seleccionar al mismo individuo en ambos casos y tras el muestreo permanecen en la población original. Los dos individuos se comparan, introduciendo en la nueva población aquel con un mejor \foreign{fitness}.

\begin{algorithm}
	\caption{Selección por torneo binario}
	\label{alg:torneo-binario}
	\KwIn{Una población $B$ de tamaño $M$}
	\KwResult{nueva población seleccionada}
	\KwData{$B'$}
	$B' := \emptyset$ \\
	\ForEach{$m$ desde $1$ hasta $M$}{
		$indA := aleatorio(B)$ \\
		$indB := aleatorio(B)$ \\
		\uIf{$fitness(indA) \geq fitness(indB)$}{
			$B' := B' \cup \set{indA}$
		}
		\Else{
			$B' := B' \cup \set{indB}$
		}
	}
	\Return{$B'$}
\end{algorithm}

\section{Cruce PMX}

El algoritmo \ref{alg:pmx} cruza la parte de la permutación de departamentos del genotipo de un individuo. Se trata de un algoritmo un tanto difícil de entender a través de su procedimiento y es mucho más fácil su comprensión a través de una ilustración como la de la figura \ref{fig:op-cruce}.

\begin{itemize}
	\item En la primera parte del algoritmo se toma nota del índice que ocupa cada uno de los departamentos, para su futuro uso.
	
	\item A continuación se toman los puntos de corte, asegurándose de que el segundo esté por detrás del primero y ambos se encuentren dentro de los índices de las secuencias.
	
	\item Finalmente se aplica el cruce, intercambiando cada uno de los elementos dentro de los puntos de corte para que la subsecuencia de $A$ pase a ser la de $B$ y viceversa, siempre manteniendo la condición de permutación y actualizando los índices de los departamentos en cada paso.
\end{itemize}

\begin{algorithm}
	\caption{Cruce \acs{PMX}}
	\label{alg:pmx}
	\KwIn{$departamentosA$, $departamentosB$}
	\KwData{$indicesA$, $indicesB$, $corte1$, $corte2$, $temp$, $tA$, $tB$}
	\tcp{Obtener el índice de cada departamento}
	$indicesA := \langle 0, 0, \dotsc, 0 \rangle$ \tcp{$n$ veces}
	$indicesB := \langle 0, 0, \dotsc, 0 \rangle$ \tcp{$n$ veces}
	\ForEach{$i$ desde $0$ hasta $n-1$}{
		$indicesA[departamentosA[i]] := i$\\
		$indicesB[departamentosB[i]] := i$
	}
	\tcp{Elegir los puntos de corte}
	$corte1 := aleatorio(\set{0, 1, \dotsc, n})$ \\
	$corte2 := aleatorio(\set{0, 1, \dotsc, n-1})$ \\
	\uIf{$corte2 \geq corte1$}{
		$corte2 := corte2 + 1$
	}
	\Else(\tcp*[h]{intercambiar cortes}){
		$temp := corte1$ \\
		$corte1 := corte2$ \\
		$corte2 := temp$
	}
	\tcp{Aplicar cruce}
	\ForEach{$i$ desde $corte1$ hasta $corte2$}{
		\tcp{Mantener constancia de los departamentos seleccionados}
		$tA := departamentosA[i]$ \\
		$tB := departamentosB[i]$ \\
		\tcp{Intercambiar los valores por su contrario}
		$departamentosA[i] := tB$ \\
		$departamentosA[indicesA[tB]] := tA$ \\
		$departamentosB[i] := tA$ \\
		$departamentosB[indicesB[tA]] := tB$ \\
		\tcp{Actualizar los índices de los departamentos}
		$temp := indicesA[tA]$ \\
		$indicesA[tA] := indicesA[tB]$ \\
		$indicesA[tB] := temp$ \\
		$temp := indicesB[tB]$ \\
		$indicesB[tB] := indicesB[tA]$ \\
		$indicesB[tA] := temp$ \\
	}
\end{algorithm}

\section{Cruce SPX}

El algoritmo \ref{alg:spx} cruza la parte de la secuencia de cortes del genotipo de un individuo. Este algoritmo se aplica junto al algoritmo \ref{alg:pmx} (\ac{PMX}) para realizar el cruce completo de un individuo.

En resumen, este algoritmo toma un punto de corte aleatorio del vector (este punto se toma entre $1$ y $n-2$ para que siempre sea efectivo, de lo contrario podría no efectuarse ninguna modificación). A continuación se intercambia una de las mitades entre cada una de las secuencias para obtener los nuevos genotipos.

\begin{algorithm}
	\caption{Cruce \acs{SPX}}
	\label{alg:spx}
	\KwIn{$cortesA$, $cortesB$}
	\KwData{$corte$, $temp$}
	$corte := aleatorio(\set{1, 2, \dotsc, n-2})$ \\
	\ForEach{$i$ desde $corte$ hasta $n-2$}{
		$temp := cortesA[i]$ \\
		$cortesA[i] := cortesB[i]$ \\
		$cortesB[i] := temp$
	}
\end{algorithm}

\section{Mutación de los individuos}

El flujo de decisión para realizar la mutación es el siguiente:

\begin{itemize}
	\item Para cada individuo de la población:
	\begin{itemize}
		\item Mutar al individuo con una probabilidad (probabilidad de mutación, parametrizada).
		\begin{itemize}
			\item Con un $50\%$ de probabilidad, mutar la permutación.
			
			\item Con un $50\%$ de probabilidad, mutar los cortes.
			\begin{itemize}
				\item Con un $50\%$ de probabilidad, invertir un valor del vector de cortes.
				
				\item Con un $50\%$ de probabilidad, desplazar un $1$ del vector de cortes a izquierda o derecha (probabilidad $50/50$).
			\end{itemize}
		\end{itemize}
		
		
		\item En caso contrario, dejar el individuo como está.
	\end{itemize}
\end{itemize}

En la figura \ref{fig:diagrama-mutacion} se muestra el proceso de mutación descrito en la sección \ref{subsec:operadores-geneticos} de forma gráfica.

\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{Figures/diagrama_mutacion.pdf}
	\caption{Diagrama de flujo representando el proceso de mutación}
	\label{fig:diagrama-mutacion}
\end{figure}

\section{Algoritmo de islas localizadas}

A continuación se analiza en detalle el diseño del algoritmo de islas localizadas. Cómo ya se explicó en el capítulo \ref{cap:especificacion} el sistema implementará una arquitectura gestor-trabajador. Una ilustración de esta arquitectura puede observarse en la figura \ref{fig:arquitectura}.

\begin{figure}[]
	\centering
	\includegraphics[width=0.5\textwidth]{Figures/diagrama_arquitectura.pdf}
	\caption{Arquitectura gestor-trabajador del algoritmo}
	\label{fig:arquitectura}
\end{figure}

Una vez lanzados los procesos trabajadores, el gestor se mantiene a la espera de algún mensaje entrante de los mismos. Cuando el trabajador $i$ realiza $C_i$ generaciones, envía un mensaje al gestor con su mejor individuo $S^1_i$ y la diversidad de su población $D(B_i)$. El gestor tratará estos mensajes según una cola \ac{FIFO}. Este proceso ejecutado por cada trabajador se plasma en el algoritmo \ref{alg:isla}.

\begin{algorithm}
	\caption{Algoritmo genético ejecutado en la isla $i$}
	\label{alg:isla}
	\KwData{$gen\_restantes$, $gen\_ciclo$, $respuesta$}
	Inicializar población $B_i$ de forma aleatoria \\
	Evaluar $B_i$ \\
	$gen\_restantes := G_i$ \\
	\While{$gen\_restantes > 0$}{
		$gen\_ciclo := \min(C_i, \; gen\_restantes)$ \\
		$gen\_restantes := gen\_restantes - gen\_ciclo$ \\
		\While{$gen\_ciclo > 0$}{
			Aplicar selección por torneo binario a $B_i$ \\
			Aplicar cruce con probabilidad $P^{cross}_i$ a $B_i$ \\
			Aplicar mutación con probabilidad $P^{mut}_i$ a $B_i$ \\
			Evaluar $B_i$ \\
			$gen\_ciclo := gen\_ciclo - 1$ \\
		}
		Ordenar $B_i$ según \foreign{fitness} \\
		Enviar $D(B_i)$ y $S^1_i$ al gestor. \\
		$respuesta := $ respuesta del gestor \\
		\If{$respuesta \neq \emptyset$}{
			$S^1_i := respuesta$
		}
	}
\end{algorithm}

El gestor guardará una copia de este individuo en su \emph{ventana de migración} correspondiente $\VM_i$ y realizará una migración si se ha alcanzado el umbral mínimo de diversidad $D_{min}$ y existe algún otro individuo disponible para migrar. En caso de que haya más de uno, \emph{elegirá el individuo disponible más lejano al recibido} para así fomentar que los individuos migren a islas lo más distintas posibles a la de origen. La formalización de este protocolo se encuentra en el algoritmo \ref{alg:migracion}.

\begin{algorithm}
	\caption{Protocolo de migración}
	\label{alg:migracion}
	\Trigger{la isla $i$ envía su mejor individuo $S^1_i$}
	\KwData{$m\acute{a}s\_lejano, mayor\_distancia$}
	\If{$D(B_i) < D_{min}$}{
		$m\acute{a}s\_lejano := \emptyset$ \\
		$mayor\_distancia := 0$ \\
		\ForEach{$j \in \set{1,\dots,I}$}{
			\If{$i \neq j \land \VM_j \neq \emptyset$}{
				\If{$d_{real}(S^1_i, \VM_j) > mayor\_distancia$}{
					$m\acute{a}s\_lejano := \VM_j$ \\
					$mayor\_distancia := d_{real}(S^1_i, \VM_j)$ \\
				}
			}
		}
		\If{$m\acute{a}s\_lejano \neq \emptyset$}{
			Enviar $m\acute{a}s\_lejano$ a la isla $i$ como emigrante
		}	
	}
\end{algorithm}

En el diagrama de secuencia de la figura \ref{fig:comunicacion} se muestra un ejemplo de comunicación entre el proceso gestor y dos hipotéticos procesos trabajadores.

\begin{figure}[]
	\centering
	\includegraphics[width=0.8\textwidth]{Figures/secuencia_gestor_trabajador.pdf}
	\caption{Ejemplo de comunicación entre procesos}
	\label{fig:comunicacion}
\end{figure}

El ejemplo se ha diseñado de forma que para la primera comunicación no se tome acción y para la segunda se decida migrar para ilustrar cada una de las situaciones. Todas las decisiones dependen del historial de ejecución hasta el momento. El proceso se realiza de forma asíncrona mediante el paso de mensajes, lo que hace la reproducibilidad del mismo imposible aún intentando controlar las semillas para las decisiones aleatorias ya que el \foreign{scheduling} del sistema operativo afectará enormemente en el orden de esta comunicación.