\chapter{Diseño}
\label{cap:diseno}

Una vez especificado el comportamiento del algoritmo se detalla a continuación el diseño de alto nivel para la implementación. Éste se describe a dos niveles:

\begin{itemize}
	\item El \myemph{diseño arquitectónico} donde se describen los módulos en los que se estructura el sistema.
	\item el \myemph{diseño procedimental} donde se especifican las clases fundamentales para la implementación del programa.
\end{itemize}

Adicionalmente, se describirán de forma técnica los \myemph{ficheros de entrada/salida} del programa.

La implementación de este diseño puede encontrarse en el Manual de Código de este proyecto.

\section{Diseño arquitectónico}

El programa utiliza una estructura en módulos al estilo de Python. Las dependencias entre estos módulos pueden observarse en la figura \ref{fig:dependencias}.

\begin{figure}[]
	\centering
	\includegraphics[width=\textwidth]{Figures/dependencias.pdf}
	\caption{Grafo de dependencias del programa}
	\label{fig:dependencias}
\end{figure}

\subsection{Módulo \module{localized\_islands}}

El módulo \module{localized\_islands} no es más que el ejecutable principal del programa. Es junto a \module{draw} el único módulo ejecutable del programa.

Al ser ejecutado debe recibir como parámetros la localización del fichero de descripción del problema (sección \ref{subsec:fichero-problema}) y del fichero de parámetros (sección \ref{subsec:fichero-params}) y generará como resultado a su finalización el fichero de soluciones (sección \ref{subsec:fichero-soluciones}).

\subsection{Módulo \module{lsa}}

En este módulo se define el funcionamiento principal del algoritmo, esto es, aquél del proceso gestor. Aquí se encuentra implementado el protocolo de migración así como la selección final de soluciones resultado.

\subsection{Módulo \module{islands}}

En \module{islands} se encuentra la implementación del comportamiento de cada proceso trabajador (a grandes rasgos, la implementación del algoritmo \ref{alg:isla}). Es entonces aquí donde se especifica el uso (no la definición) de los distintos operadores genéticos, así como la comunicación con el proceso gestor.

\subsection{Módulo \module{fbs}}

El módulo \module{fbs} engloba la especificación de la estructura \ac{FBS} así como las distintas operaciones sobre la misma (es decir, sus operadores genéticos): creación, cruce, mutación...

Así mismo contiene la lógica de evaluación, tanto aquella con penalización como sin ella.

\subsection{Módulo \module{misc}}

Este módulo contiene las medidas utilizadas por el algoritmo en forma de funciones: distancia entre individuos y diversidad.

\subsection{Módulo \module{geometry}}

En el módulo \module{geometry} se especifican, como su nombre indica, aquellas medidas y transformaciones geométricas sobre las soluciones al \ac{FLP}: los distintos tipos de distancias y el cálculo de centroides de los departamentos.

\subsection{Módulo \module{fldata}}

Este módulo gestiona la entrada/salida de datos del programa proveyendo una interfaz para la lectura y escritura de los ficheros definidos en la sección \ref{sec:ficheros}.

\subsection{Módulo \module{draw}}

El único otro módulo ejecutable a parte de \module{localized\_islands} es \module{draw}, que permite generar representaciones de las soluciones así como distintas gráficas sobre la ejecución del algoritmo, con una variedad de opciones para ello.

\subsection{Diagrama de despliegue}

Haciendo uso de estos módulos, el despliegue ejecución del programa principal se realizará según el modelo de la figura \ref{fig:diagrama-despliegue}. La comunicación entre procesos se realizará mediante el protocolo del módulo \module{pickle} de Python. Sobre éste se implementará el protocolo de migración definido en el algoritmo \ref{alg:migracion}. Los componentes trabajadores serán implementados en la clase \class{WorkerIsland} especificada en la sección \ref{subsec:worker-island} y el componente gestor en la clase \class{LocalizationStrategyAlgorithm} de la sección \ref{subsec:manager}.

\begin{figure}[]
	\centering
	\includegraphics[width=\textwidth]{Figures/despliegue.pdf}
	\caption{Diagrama de despliegue}
	\label{fig:diagrama-despliegue}
\end{figure}

\section{Diseño procedimental}

En esta sección se especificarán los detalles de la estructura e interfaz de las clases que componen el programa. Se detallarán aquellos atributos y métodos relevantes para su funcionamiento, así como cualquier otra información adicional. Una visión general de las clases se puede observar en el diagrama de clases de la figura \ref{fig:diagrama-clases}.


\begin{figure}[]
	\centering
	\begin{adjustbox}{center}
	\includegraphics[width=1.2\textwidth]{Figures/clases.pdf}
	\end{adjustbox}
	\caption{Diagrama de clases}
	\label{fig:diagrama-clases}
\end{figure}

\subsection{Clase \class{DataMaster}}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.3\textwidth]{Figures/classes/DataMaster.pdf}
	\caption{Clase \class{DataMaster}}
\end{figure}

La clase \class{DataMaster} aporta la funcionalidad genérica de entrada/salida para el resto de clases del módulo \module{fldata} (secciones \ref{subsec:class-problem}, \ref{subsec:class-parameters} y \ref{subsec:class-solutions}).

\begin{itemize}
	\item Localización: módulo \module{fldata}.
	\item Métodos:
	\begin{itemize}
		\item \method{load}: Carga la información relativa a la estructura almacenada en un fichero.
		\item \method{save}: Guarda esta misma información en un fichero.
		\item \method{build} (virtual): Puebla los atributos del objeto dada una estructura válida.
	\end{itemize}
\end{itemize}

\subsection{Clase \class{Problem}} \label{subsec:class-problem}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Figures/classes/Problem.pdf}
	\caption{Clase \class{Problem}}
\end{figure}

La clase \class{Problem} es una representación estática de todos los datos de una instancia concreta del \ac{FLP}.

\begin{itemize}
	\item Localización: módulo \module{fldata}.
	\item Atributos:
	\begin{itemize}
		\item \attr{problemname}: Nombre del problema, una etiqueta para reconocerlo fácilmente.
		\item \attr{distance}: Tipo de distancia utilizada por el problema (euclidiana, de Manhattan o de Chebyshev).
		\item \attr{floors}: Lista de plantas que componen el problema. El programa sólo aceptará problemas de una única planta.
		\item \attr{departments}: Lista de departamentos, con todos los datos relativos a cada uno de ellos (especificados en la sección \ref{subsec:datos-entrada}).
		\item \attr{tables}: Tablas con la información de flujo de materiales entre departamentos.
	\end{itemize}
\end{itemize}

\subsection{Clase \class{Parameters}} \label{subsec:class-parameters}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/classes/Parameters.pdf}
	\caption{Clase \class{Parameters}}
\end{figure}

La clase \class{Parameters} representa los parámetros de ejecución del algoritmo.

\begin{itemize}
	\item Localización: módulo \module{fldata}.
	\item Atributos:
	\begin{itemize}
		\item \attr{parameters}: Diccionario que contiene todos los pares clave/valor de los parámetros del algoritmo.
	\end{itemize}
\end{itemize}

\subsection{Clase \class{Solutions}} \label{subsec:class-solutions}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{Figures/classes/Solutions.pdf}
	\caption{Clase \class{Solutions}}
\end{figure}

La clase \class{Solutions} representa los parámetros de ejecución del algoritmo.

\begin{itemize}
	\item Localización: módulo \module{fldata}.
	\item Atributos:
	\begin{itemize}
		\item \attr{problemname}: Nombre de la instancia del problema a la que son relativas las soluciones.
		\item \attr{parameters}: Diccionario de parámetros de la ejecución algoritmo.
		\item \attr{solutions}: Conjunto de soluciones obtenidas por el algoritmo.
		\item \attr{datetime}: Fecha de generación de las soluciones.
		\item \attr{processor}: Identificador del modelo del procesador de la máquina donde se ha ejecutado el programa.
		\item \attr{cpu\_time}: Tiempo total tomado por la ejecución del algoritmo.
		\item \attr{intra\_diversity}: Historial de la evolución de la diversidad interna de cada isla.
		\item \attr{inter\_diversity}: Historial de la evolución de la diversidad entre las distintas islas.
		\item \attr{migration\_matrix}: Matriz de migraciones, que especifica cuántos individuos han migrado desde cada isla a cada otra isla.
	\end{itemize}
\end{itemize}

\subsection{Clase \class{LocalizationStrategyAlgorithm}} \label{subsec:manager}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/classes/LocalizationStrategyAlgorithm.pdf}
	\caption{Clase \class{LocalizationStrategyAlgorithm}}
\end{figure}

La clase \class{LocalizationStrategyAlgorithm} modela el flujo principal del algoritmo de islas localizadas tal y como se define en la sección \ref{sec:islas-localizadas}, es decir, la parte del \emph{proceso gestor}.

\begin{itemize}
	\item Localización: módulo \module{lsa}.
	\item Métodos:
	\begin{itemize}
		\item \method{run}: Dado un problema y unos parámetros, ejecutar el algoritmo de islas localizadas. Da como resultado un objeto de la clase \class{Solutions}. Una ilustración de este proceso como máquina de estados puede observarse en la figura \ref{fig:proceso-gestor}.
	\end{itemize}
\end{itemize}

\begin{figure}[]
	\centering
	\includegraphics[width=0.9\textwidth]{Figures/estados_gestor.pdf}
	\caption{Diagrama de estados del proceso gestor}
	\label{fig:proceso-gestor}
\end{figure}

\subsection{Clase \class{WorkerIsland}} \label{subsec:worker-island}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\textwidth]{Figures/classes/WorkerIsland.pdf}
	\caption{Clase \class{WorkerIsland}}
\end{figure}

La clase \class{WorkerIsland} es la implementación de cada uno de los procesos trabajadores. El método \method{run} de \class{LocalizationStrategyAlgorithm} lanza tantos procesos como se hayan especificado en los parámetros. Para la generación de estos procesos es necesario proveer los parámetros específicos para su ejecución.

\begin{itemize}
	\item Localización: módulo \module{islands}.
	\item Métodos:
	\begin{itemize}
		\item \method{run}: Comienza la ejecución del proceso trabajador. Al igual que el proceso gestor, puede observarse una ilustración de los procesos trabajadores como máquina de estados en la figura \ref{fig:proceso-trabajador}.
	\end{itemize}
\end{itemize}

\begin{figure}[]
	\centering
	\includegraphics[width=0.9\textwidth]{Figures/estados_trabajador.pdf}
	\caption{Diagrama de estados del proceso trabajador}
	\label{fig:proceso-trabajador}
\end{figure}

\subsection{Clase \class{FBS}}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.25\textwidth]{Figures/classes/FBS.pdf}
	\caption{Clase \class{FBS}}
\end{figure}

La clase \class{FBS}  contiene toda la información relativa a un individuo y provee aquellas operaciones necesarias para su evolución (mutación y cruce).

\begin{itemize}
	\item Localización: módulo \module{fbs}.
	\item Atributos:
	\begin{itemize}
		\item \attr{fac}: Secuencia de departamentos, implementada como una lista.
		\item \attr{cut}: Vector de cortes, implementado como una lista.
		\item \attr{fitness}: Valor de aptitud del individuo.
	\end{itemize}

	\item Métodos:
	\begin{itemize}
		\item \method{mutate}: Muta al individuo según el proceso especificado en la sección \ref{subsec:operadores-geneticos} (figura \ref{fig:diagrama-mutacion}). Esta operación modifica al objeto.
		\item \method{crossover}: Cruza al individuo con otro según se especifica en la sección \ref{subsec:operadores-geneticos} (figura \ref{fig:op-cruce}). Esta operación modifica a ambos objetos.
	\end{itemize}
\end{itemize}

\subsection{Clase \class{FlowEvaluator}}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{Figures/classes/FlowEvaluator.pdf}
	\caption{Clase \class{FlowEvaluator}}
\end{figure}

La clase \class{FlowEvaluator} se utiliza para evaluar a los individuos atendiendo exclusivamente a la matriz de coste de flujo entre departamentos.

\begin{itemize}
	\item Localización: módulo \module{fbs}.
	\item Atributos:
	\begin{itemize}
		\item \attr{problem}: Instancia del problema cuyas soluciones se evaluarán (objeto \class{Problem}).
		\item \attr{distance}: Función de distancia utilizada en el problema (euclidiana, de Manhattan o de Chebyshev).
	\end{itemize}

	\item Métodos:
	\begin{itemize}
		\item \method{\_\_call\_\_}: Llamar al objeto como una función y aportándole un individuo \class{FBS} da como resultado la suma total del coste del flujo de materiales como medida de aptitud (ecuación \ref{eq:fitness-material-flow}).
	\end{itemize}
\end{itemize}

\subsection{Clase \class{FlowWithAdaptativePenaltyEvaluator}}

\begin{figure}[H]
	\centering
	\includegraphics[width=0.5\textwidth]{Figures/classes/FlowWithAdaptativePenaltyEvaluator.pdf}
	\caption{Clase \class{FlowWithAdaptativePenaltyEvaluator}}
\end{figure}

La clase \class{FlowWithAdaptativePenaltyEvaluator} se utiliza para evaluar a los individuos teniendo en cuenta tanto el coste del flujo de materiales entre departamentos como aplicando una penalización según la relación de aspecto de los mismos.

\begin{itemize}
	\item Localización: módulo \module{fbs}.
	\item Atributos:
	\begin{itemize}
		\item \attr{problem}: Instancia del problema cuyas soluciones se evaluarán (objeto \class{Problem}).
		\item \attr{distance}: Función de distancia utilizada en el problema (euclidiana, de Manhattan o de Chebyshev).
		\item \attr{best\_overall\_flow}: Mejor valor de coste de flujo de materiales total encontrado hasta ahora.
		\item \attr{best\_feasible\_flow}: Mejor valor de coste de flujo de materiales total encontrado hasta ahora atendiendo únicamente a las soluciones válidas.
	\end{itemize}

	\item Métodos:
	\begin{itemize}
		\item \method{\_\_call\_\_}: Llamar al objeto como una función y aportándole un individuo \class{FBS} da como resultado la suma total del coste del flujo de materiales más el valor de penalización como medida de aptitud (ecuación \ref{eq:fitness}).
	\end{itemize}
\end{itemize}

\section{Diseño de ficheros de entrada/salida} \label{sec:ficheros}

El programa carga y guarda todos los datos en ficheros estructurados según la sintaxis de Python. Para su evaluación se hace uso del módulo \module{ast} de la librería estándar, lo que facilita y acelera enormemente el proceso. Además, permite hacer uso de las estructuras nativas del lenguaje como listas, tuplas y cadenas de caracteres.

Todos los ficheros se estructuran como una \emph{tupla} en su raíz. Ya que el módulo \module{fldata} ha sido utilizado con anterioridad en una clase más amplia de problemas existe numerosa funcionalidad que no es relevante a este proyecto, así que se omitirá esa información y se comentarán tan sólo aquellos datos relevantes. En cada punto \emph{se numera la posición en la tupla de ese dato}.

\subsection{Fichero de problema} \label{subsec:fichero-problema}

Se describe a continuación los ficheros que contienen la información relativa a una instancia del problema \ac{UA-FLP}, que será gestionado por la clase \class{Problem} (sección \ref{subsec:class-problem}).

\begin{itemize}
	\item [{[0]}] \class{str}: Descripción general del problema e información relevante en formato legible por humanos.
	\item [{[1]}] \class{str}: Tipo de distancia utilizada por el problema. Debe tener como valor uno de los siguientes: \{\code{'Euclidean', 'Manhattan', 'Chebyshev'}\}
	\item [{[2]}] \class{tuple}: Lista de plantas (el programa sólo tratará con problemas de una sola planta). Cada uno de los elementos será a su vez una tupla describiendo cada planta y contendrá lo siguiente:
	\begin{itemize}
		\item [{[1]}] \class{list}: Vértices de la planta en formato $(x,y)$ (el programa sólo tratará con plantas rectangulares).
	\end{itemize}

	\item [{[3]}] \class{tuple}: Lista de departamentos. Cada uno de los elementos será a su vez una tupla describiendo cada uno de los departamentos y contendrá lo siguiente:
	\begin{itemize}
		\item [{[0]}] \class{str}: Nombre descriptivo del departamento.
		\item [{[2]}] \class{float}: Área necesaria para el departamento ($A_i$).
		\item [{[7]}] \class{float}: Relación de aspecto máxima del departamento (${\alpha_{max}}_i$).
	\end{itemize}

	\item [{[4]}] \class{dict}: Diccionario de tablas de relación. Cada tabla (matriz) se implementa como lista de listas. Puede contener las siguientes claves, cada una relativa a un tipo de tabla:
	\begin{itemize}
		\item [\code{'Flow'}:] Cuantifica el flujo de material entre cada departamento. Puede aparecer junto a \code{'Cost'} de forma que el coste total sea la multiplicación elemento a elemento de ambas. Si aparece por sí sola se considera un coste unitario de desplazamiento de material entre todos los departamentos.
		\item [\code{'Cost'}:] Cuantifica el coste de desplazamiento de material por unidad de distancia entre cada departamento. Debe aparecer junto a \code{'Flow'}.
		\item [\code{'CostFlow'}:] Cuantifica el coste total por desplazamiento de materiales por unidad de distancia entre cada departamento. Únicamente puede aparecer por sí misma.
	\end{itemize}
\end{itemize}

\subsection{Fichero de parámetros} \label{subsec:fichero-params}

El fichero de parámetros determina los distintos parámetros de ejecución del algoritmo mencionados en la sección \ref{subsec:datos-entrada}.

\begin{itemize}
	\item [{[0]}] \class{str}: Nombre del algoritmo al que pertenecen estos parámetros.
	\item [{[2]}] \class{dict}: Diccionario de parámetros. Cada par clave/valor determina un parámetro distinto:
	\begin{itemize}
		\item [] \code{'n\_workers'} (\class{int}): Número de procesos trabajadores (islas) a lanzar ($I$).
		\item [] \code{'n\_best\_keep\_per\_island'} (\class{int}): Número de mejores individuos a guardar por número de islas ($n_{keep}$).
		\item [] \code{'diversity\_threshold'} (\class{float}): Umbral mínimo de diversidad en las islas ($D_{min}$).
		\item [] \code{'workers\_params\_range'} (\class{dict}): Rangos en los que se generarán aleatoriamente cada uno de los parámetros de cada isla. Es a su vez otro diccionario en el que cada valor es una tupla de dos elementos: el rango mínimo y el máximo respectivamente:
		\begin{itemize}
			\item [] \code{'number\_of\_generations'} (\class{int}): Número de generaciones total $G_i$.
			\item [] \code{'number\_of\_generations\_migrate'} (\class{int}): Número de generaciones entre migraciones ($C_i$).
			\item [] \code{'population\_size\_island'} (\class{int}): Tamaño de la población en la isla ($M_i$).
			\item [] \code{'mutation\_prob'} (\class{float}): Probabilidad de mutación ($P^{mut}_i$).
			\item [] \code{'crossover\_prob'} (\class{float}): Probabilidad de cruce ($P^{cross}_i$).
		\end{itemize}
	\end{itemize}
\end{itemize}

\subsection{Fichero de soluciones} \label{subsec:fichero-soluciones}

El fichero de soluciones es el resultado final del algoritmo. Almacena datos sobre la ejecución y las soluciones finales del mismo, como se indica en la sección \ref{subsec:datos-salida}.

\begin{itemize}
	\item [{[0]}] \class{str}: Nombre de la instancia del problema.
	\item [{[1]}] \class{tuple}: Información de los parámetros del algoritmo tal y como se describen en la sección \ref{subsec:fichero-params}. A lo descrito ahí se añaden los datos concretos (generados aleatoriamente) de cada uno de los trabajadores (clave \code{'woker\_params'}).
	\item [{[2]}] \class{list}: Cabecera de cada uno de los valores de aptitud listados. Las soluciones finales del programa sólo contendrán su valor de aptitud en base al coste del flujo de materiales (obtenido por el evaluador \class{FlowEvaluator}).
	\item [{[3]}] \class{list}: Lista de soluciones finales. Cada elemento de la lista es a su vez una tupla que representa una única solución:
	\begin{itemize}
		\item [{[1]}] \class{tuple}: Valores de aptitud. En el programa sólo tendrá un elemento \class{float}.
		\item [{[2]}] \class{str}: Cadena describiendo el genotipo del individuo.
		\item [{[4]}] \class{list}: Lista de departamentos (fenotipo). Cada elemento es a su vez una tupla:
		\begin{itemize}
			\item [{[0]}] \class{str}: Nombre del departamento.
			\item [{[3]}] \class{tuple}: Vértices del departamento en formato $(x,y)$.
		\end{itemize}
	\end{itemize}
	\item [{[4]}] \class{str}: Fecha y hora de generación del fichero.
	\item [{[5]}] \class{str}: Nombre de la máquina donde se ha ejecutado el programa (\foreign{host}).
	\item [{[6]}] \class{str}: Información del procesador de la máquina \foreign{host}.
	\item [{[7]}] \class{float}: Tiempo en segundos de ejecución del programa.
	\item [{[8]}] \class{list}: Historial de diversidad intra-isla.
	\item [{[9]}] \class{list}: Historial de diversidad inter-isla.
	\item [{[10]}] \class{list}: Matriz de migraciones entre islas.
\end{itemize}
