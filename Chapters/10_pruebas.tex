\chapter{Pruebas} \label{cap:pruebas}

En el siguiente capítulo se explicarán los procedimientos llevados a cabo para asegurar el correcto funcionamiento del algoritmo desarrollado.

Se utilizará como referencia el diseño especificado en el capítulo \ref{cap:diseno} para decidir si el programa se comporta de forma adecuada, haciendo uso de pruebas de distintos tipos.

Gracias a este paso se podrá confiar en mayor medida en el buen funcionamiento del algoritmo, aunque sea imposible garantizar que esté completamente libre de errores. En cualquier caso, las pruebas de funcionamiento ayudarán a evitar cierta clase de fallos y \foreign{bugs} que surjan durante el desarrollo.

Todas las pruebas ejecutadas serán pruebas de unidad sobre las distintas clases principales del sistema, es decir, se comprobará el correcto funcionamiento de cada una de las partes sin prestar atención a su integración a nivel modular o de sistema.

Todas las pruebas se implementarán utilizando el módulo \module{unittest} de la librería estándar de Python 3\footnote{\foreign{The Python Standard Library}: \module{unittest} -- \foreign{Unit testing framework} \url{https://docs.python.org/3.4/library/unittest.html}}. Haciendo uso de este módulo cada uno de los tests se agrupan bajo un <<caso de test>> o \code{TestCase}.

\section{Pruebas de caja blanca}

Las pruebas estructurales o de caja blanca tratan de asegurar que todos los posibles flujos de ejecución de las partes del código funcionan correctamente. Se han aplicado pruebas de este tipo a los componentes críticos del sistema para asegurar su funcionamiento.

\begin{itemize}
	\item Tests sobre la representación de soluciones \ac{FBS}:
	
	\begin{itemize}
		\item[\testlabel{CB-1}] Comprobar que la permutación de departamentos y la secuencia de cortes de las soluciones generadas aleatoriamente son válidas.
		
		Para ello se generan 1000 soluciones aleatorias para el problema de prueba y se comprueban los resultados.
		
		\item[\testlabel{CB-2}] Comprobar que las poblaciones se generan de forma correcta.
		
		Se generan poblaciones de 1, 2, 10, 20, 1000 y 3000 individuos, así como otras 10 de tamaños distintos generados aleatoriamente (menores de 5000). Se comprueba que la cantidad de individuos generados es correcta. A su vez, se comprueba que los individuos generados son correctos al igual que en \testlabel{CB-1}.
		
		\item[\testlabel{CB-3}] Comprobar que la mutación genera individuos válidos.
		
		Se genera un individuo y se muta 100 veces, comprobando en cada mutación que sigue siendo válido. Este proceso se repite a su vez 100 veces con nuevos individuos iniciales.
		
		\item[\testlabel{CB-4}] Comprobar que el cruce genera individuos válidos.
		
		Se generan dos individuos y se cruzan 100 veces, comprobando en cada cruce que ambos siguen siendo válidos. Este proceso se repite a su vez 100 veces con nuevos individuos iniciales.
	\end{itemize}
	
	\item Tests sobre la evaluación:
	\begin{itemize}
		\item[\testlabel{CB-5}] Comprobar que el \foreign{fitness} calculado es válido.
		
		Se generan 1000 individuos y se comprueba que la evaluación da resultados con la estructura correcta y mayores o iguales a 0.
		
		\item[\testlabel{CB-6}] Comprobar que la penalización se aplica correctamente a la evaluación.
		
		Se genera una población de 100 individuos que se evalúa con el evaluador con penalización para que tenga unos datos de los que partir. A continuación se generan 100 individuos nuevos y se evalúan con este mismo evaluador, comprobando que su \foreign{fitness} sin penalización es siempre menor o igual a aquél con penalización. Este proceso se repite 100 veces.
	\end{itemize}
	
	\item Tests sobre los procesos trabajadores:
	\begin{itemize}
		\item[\testlabel{CB-7}]  Comprobar que los procesos trabajadores responden de forma correcta al protocolo de migración y que finalizan correctamente.
		
		Se lanza un proceso trabajador de prueba con parámetros genéricos y se simula la comunicación con el proceso gestor. Se controla que los mensajes son válidos en todo momento y que se finaliza de forma correcta. También se comprueba que la cantidad de ciclos de migración es la esperada según los parámetros.
	\end{itemize}
\end{itemize}

\section{Pruebas de caja negra}

Las pruebas de caja negra atienden únicamente a la interfaz de las unidades puestas a prueba, es decir, comprueban que las unidades de testeo dan los resultados correctos dadas unas ciertas entradas predefinidas.

El único test de caja negra realizado es un test sobre los resultados del algoritmo:

\begin{itemize}
	\item[\testlabel{CN-1}] Comprobar que, dados unos parámetros y un problema, las soluciones generadas son válidas.
	
	Se ejecuta el algoritmo utilizando un problema y unos parámetros de prueba. Una vez se obtienen las soluciones, se comprueba que los parámetros de ejecución de cada una de las islas se encuentran dentro del rango definido en el fichero de parámetros y que el número de soluciones obtenidas es coherente.
\end{itemize}

\section{Ejecución de las pruebas}

Puede comprobarse que el \foreign{software} pasa las pruebas ejecutando el fichero \pathname{test.py} del código adjunto. Estos tests hacen uso del fichero de parámetros \pathname{test.params} y el problema de prueba \pathname{problems/test\_problem.py}.

Se asegurará que el código entregado pasa todas las pruebas definidas en este capítulo.