\chapter{Experimentación}

\label{cap:experimentacion}

Para medir la efectividad del algoritmo se han diseñado una serie de pruebas experimentales que determinen los parámetros óptimos de ejecución del algoritmo.

La principal dificultad presente es que no existe un intento anterior en la literatura de medir la diversidad de las soluciones obtenidas, así que no hay disponibles datos comparativos en este aspecto. Sin embargo, sí que se tendrán en cuenta los valores de \foreign{fitness} alcanzados.

Debido al alto tiempo de ejecución de cada prueba y el gran número de problemas disponibles, se ha decidido realizar las pruebas en dos fases distintas:

\begin{enumerate}
	\item Se harán pruebas paramétricas con 3 problemas significativos distintos que se utilizarán para escoger una configuración óptima.
	\item Se utilizarán los parámetros obtenidos en la fase anterior para probar el algoritmo con el resto de problemas.
\end{enumerate}

\section{Primera fase: estudio paramétrico} \label{sec:primera-fase}

Durante esta primera fase se utilizarán los siguientes tres problemas, elegidos por las razones que se especifican a continuación:

\begin{itemize}
	\item \problem{Slaughterhouse}: Escogido como problema realista estándar, que utiliza distancia euclidiana a diferencia de los otros dos. Extraído de \cite{Salas-Morera1996}.
	\begin{itemize}
		\item Dimensiones de la planta (ancho \times\ alto): \num{51.14 x 30}
		\item Distancia utilizada: Euclidiana.
		\item Número de departamentos: 12
		\item Ratio de aspecto máximo: 4 (todos los departamentos)
	\end{itemize}
	
	\item \problem{AB20\_AR2}: Escogido en su versión con un ratio de aspecto ${\alpha_{max}}_i = 2$ para todos los departamentos, para estudiar el comportamiento del algoritmo con problemas con un ratio de aspecto más restrictivo. Extraído de \cite{KOMARUDIN2010730}, aunque la versión utilizada ahí es de un ratio de aspecto menos restrictivo (para la comparativa se utilizará la versión original, ver la siguiente fase).
	\begin{itemize}
		\item Dimensiones de la planta (ancho \times\ alto): \num{2 x 3}
		\item Distancia utilizada: Manhattan
		\item Número de departamentos: 20
		\item Ratio de aspecto máximo: 2 (todos los departamentos)
	\end{itemize}
	
	\item \problem{SC35}: Escogido por su gran número de departamentos y huecos, ya que esto implica un mayor espacio de búsqueda. Extraído de \cite{KOMARUDIN2010730}.
	\begin{itemize}
		\item Dimensiones de la planta (ancho \times\ alto): \num{16 x 15}
		\item Distancia utilizada: Manhattan
		\item Número de departamentos: 35 además de 24 huecos vacíos (representados como departamentos de flujo nulo al resto).
		\item Ratio de aspecto máximo: 4 en los departamentos, ilimitado en los huecos.
	\end{itemize}
\end{itemize}

\subsection{Configuraciones a probar}

Para cada uno de estos tres problemas se ejecutarán todas las combinaciones posibles de valores que pueden observarse en la tabla \ref{tbl:parametros}. Los rangos intermedios se han obtenido a partir de los valores recomendados de la literatura. Según éstos, se han elegido un rango más restrictivo y otro más amplio para probar el efecto de la diversidad de valores en la efectividad del algoritmo.

\input{Tables/parameters.tex}

Cada configuración se ejecutará 5 veces. En total se ejecutarán \num[exponent-base=3, input-exponent-markers=b]{b5} combinaciones \times\ 5 repeticiones \times\ 3 problemas = 3645 ejecuciones. 

Todos los datos de cada ejecución serán almacenados, así como un resumen de las $5$ ejecuciones en el que se mostrará la media de las siguientes medidas:
\begin{itemize}
	\item Número de soluciones
	\item Aptitud media de las soluciones
	\item Suma de distancias entre pares de soluciones (diversidad total)
	\item Tiempo total de ejecución
\end{itemize}

\subsection{Ponderación de resultados y elección de mejor configuración}

La elección de la mejor configuración se realizará en base a las siguientes medidas, a las que se asignará una ponderación según su importancia:

\begin{itemize}
	\item La mejor aptitud de las soluciones obtenidas: \SI{20}{\percent}
	\item La aptitud mediana de las soluciones obtenidas: \SI{30}{\percent}
	\item La distancia mediana de las soluciones obtenidas: \SI{50}{\percent}
\end{itemize}

Se calculará para cada configuración la media de estas medidas en cada una de las 5 ejecuciones. Se elegirá también el mejor valor de cada medida y se utilizará como referencia. Para una medida $D$, cuyo mejor valor es $D_{best}$ y su valor para la configuración $k$ es $D_k$, se obtendría una puntuación:
\begin{equation}
	S^D_k = \frac{\left | D_k - D_{best} \right |}{D_k}
\end{equation}

Y para obtener la puntuación total con una ponderación $w_D$ para la medida $D$:
\begin{equation}
	S_k = \sum_D \left ( \frac{\left | D_k - D_{best} \right |}{D_k} \cdot w_D \right )
\end{equation}

Para obtener la puntuación final de cada configuración, se realiza la media de la misma para cada uno de los tres problemas. La mejor configuración será entonces aquella más cercana a una puntuación del \SI{0}{\percent}

\section{Segunda fase: comparativa}

A continuación se utilizará la mejor configuración obtenida en el paso anterior y se aplicará a una batería de problemas extraída del repositorio común del Área de Proyectos de Ingeniería. Un resumen de los problemas utilizados puede observarse en la tabla \ref{tbl:problemas}.

\input{Tables/problems.tex}

Se utilizará como referencia de los resultados aquellos de Palomo-Romero, Salas-Morera y García-Hernández \cite{PalomoRomero2017151}, que a su vez toma como referencia las publicaciones de Kulturel-Konak \cite{KULTURELKONAK2012614}, Kulturel-Konak y Konak \cite{KULTURELKONAK2011B}, Wong y Komarudin \cite{WONG20105523} y Konak et al. \cite{KONAK2006660}. Adicionalmente, se utilizarán los datos presentados en el proyecto de fin de carrera de Laura Gata Moreno \cite{GataMoreno} como referencia para los problemas \problem{ChoppedPlastic}, \problem{CartonPacks} y \problem{Slaughterhouse}, ya que no existen fuentes sobre los mismos en la literatura.

Nótese que estos datos se utilizarán unicamente para comprobar que el algoritmo ofrece resultados cercanos a los de la literatura, aunque no los supere.

El algoritmo se ejecutará 5 veces con cada problema utilizando la mejor configuración obtenida y se tomará como \foreign{fitness} de cada ejecución la media de \foreign{fitness} de todas las soluciones obtenidas. Finalmente, se tomará la media de las 5 ejecuciones como valor final para la comparativa.