Repositorio de código para el algoritmo genético diversificador (Trabajo de Fin de Grado)

Ficheros principales:

	- localized_islands.py : Programa principal. Es necesario pasarle como argumento el fichero del problema y el de los parámetros.

	- lsa.py : Algoritmo con estrategia de localización.

	- localized.params : Parámetros por defecto con los que se lanzará el algoritmo.

	- draw.py : Dibuja diversos gráficos a partir de la información de las soluciones que genera el algoritmo.

	- problems/ : Directorio con los problemas del UA-FLP.

Otros ficheros:

	- fbs.py : Código relativo a los individuos de la población (mutación, cruce, vecindarios, evaluación...)

	- islands.py : Código ejecutado en cada una de las islas.

	- misc.py : Algunas medidas útiles para el algoritmo (distancia, diversidad...).

	- fldata.py : Gestión de entrada/salida de datos (problemas, soluciones, etc).
	
	- geometry.py : Operaciones geométricas sobre individuos FBS.
