#!/usr/bin/env python3
"""
Draws the solution for a UA-FLP solution stored in the flexible bay format
"""

import argparse
import math
from pathlib import Path

import numpy as np
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import matplotlib.pyplot as plt

import fldata


def arg_parsing():
    parser = argparse.ArgumentParser()
    parser.add_argument('--problem' , '-p', type=Path, required=True)
    parser.add_argument('--solution', '-s', type=Path, required=True)
    parser.add_argument('--textsize', '-t', type=float, default=5)
    parser.add_argument('--hide-flow', '-hf', action='store_false', dest='show_flow')
    parser.add_argument('--hide-axes', '-ha', action='store_false', dest='show_axes')
    parser.add_argument('--hide-fitness', '-ho', action='store_false', dest='show_fitness')

    args = parser.parse_args()

    if not args.solution.is_file():
        parser.error("Solution '%s' does not exist" % str(args.solution))
    if not args.problem.is_file():
        parser.error("Problem '%s' does not exist" % str(args.problem))

    return args

def exagerate_alpha(alpha):
    # Exagerates the arrows alpha value using the logistic function.
    # Input value alpha in the range [0, 1]
    rango = 5
    alpha = (alpha*rango) - (rango/2)
    return 1 / (1 + math.exp(-alpha))

def centroid(vertexes):
    cx = sum((v[0] for v in vertexes)) / len(vertexes)
    cy = sum((v[1] for v in vertexes)) / len(vertexes)
    return cx, cy

def plot_intra_island_diversity(diversity, filename_out):
    n_islands = len(diversity)
    fig = plt.figure()
    ax = plt.axes()
    lines = list()

    for i in range(n_islands):
        ts, ds = zip(*diversity[i])
        lines.append(ax.plot(ts, ds, linewidth=1))

    ax.set_xlabel('Tiempo (s)')
    ax.set_ylabel('Diversidad')

    fig.savefig(filename_out)

def plot_inter_island_diversity(diversity, filename_out):
    n_islands = len(diversity[0][1])
    fig = plt.figure()
    ax = plt.axes()
    lines = list()
    ts, values = zip(*diversity)

    for i in range(n_islands):
        ds = [d[i] for d in values]
        lines.append(ax.plot(ts, ds, linewidth=1))

    ax.set_xlabel('Tiempo (s)')
    ax.set_ylabel('Distancia mediana al resto')

    fig.savefig(filename_out)

def offset_point(src, dst, r, theta):
    direction = (dst[0]-src[0], dst[1]-src[1])
    r = r*math.sqrt(sum(e**2 for e in direction))
    base_angle = math.atan2(direction[1], direction[0])
    offset_angle = base_angle + theta

    new_x = src[0] + r * math.cos(offset_angle)
    new_y = src[1] + r * math.sin(offset_angle)
    return new_x, new_y

def offset_points(src, dst, r, theta):
    return offset_point(src, dst, r, theta), offset_point(dst, src, r, -theta)

def draw_arrow(src, dst, cf, norm_cf, axes):
    src, dst = offset_points(src, dst, 0.05, math.radians(10))
    arrow_width = norm_cf# / 2
    size = dst[0]-src[0], dst[1]-src[1]

    arrow_kwargs = {
        'width' : arrow_width,
        'head_width' : arrow_width*1.3,
        'alpha' : exagerate_alpha(norm_cf),
        'shape' : 'right',
        'linewidth' : 0.01,
        'length_includes_head': True,
    }
    axes.arrow(src[0], src[1], size[0], size[1], **arrow_kwargs)

def save_solution(problem, solution, filename_out, textsize, show_flow=True, show_axes=True, show_fitness=True):
    fitness = solution[1][0]
    rooms = solution[4]

    xs = list()
    ys = list()
    for _, _, _, vertexes in rooms:
        for vertex in vertexes:
            xs.append(vertex[0])
            ys.append(vertex[1])

    xmin, xmax = min(xs), max(xs)
    ymin, ymax = min(ys), max(ys)
    xsize = xmax - xmin
    ysize = ymax - ymin
    marginx = xsize*0.1
    marginy = ysize*0.1
    xmaxlim = xmax + marginx
    xminlim = xmin - marginx
    ymaxlim = ymax + marginy
    yminlim = ymin - marginy

    room_polygons = PatchCollection([Polygon(np.array(vertexes)) for _, _, _, vertexes in rooms])
    room_polygons.set_facecolor('w')
    room_polygons.set_edgecolor('k')
    room_polygons.set_linewidth(1)

    room_centroids = {name : centroid(vertexes) for name, _, _, vertexes in rooms}

    fig, ax = plt.subplots()
    plt.axis('equal')
    ax.add_collection(room_polygons)
    ax.set_xlim(xminlim, xmaxlim)
    ax.set_ylim(yminlim, ymaxlim)

    for name, ctd in room_centroids.items():
        ax.annotate(name, ctd, horizontalalignment='center', verticalalignment='center', size=textsize)

    if show_fitness:
        ax.annotate(str(fitness), (xmax, ymax))

    plt.axis('on' if show_axes else 'off')

    # Dibujar flujos entre habitaciones
    if show_flow:
        cf_table = problem.tables['CostFlow']
        max_cf = max([max(cf_row) for cf_row in cf_table])
        normalized_cf = [[cf/max_cf for cf in cf_row] for cf_row in cf_table]
        n_rooms = len(rooms)
        for i in range(n_rooms):
            src_room_name = rooms[i][0]
            src_centroid = room_centroids[src_room_name]

            for j in range(n_rooms):
                if i == j: continue
                dst_room_name = rooms[j][0]
                dst_centroid = room_centroids[dst_room_name]
                cf = cf_table[i][j]

                if cf > 0:
                    norm_cf = normalized_cf[i][j]
                    draw_arrow(src_centroid, dst_centroid, cf, norm_cf, ax)

    fig.savefig(filename_out, bbox_inches='tight')

if __name__ == '__main__':
    args = arg_parsing()
    solutions = fldata.Solutions(str(args.solution))
    problem = fldata.Problem(str(args.problem))
    savedir = (args.solution / '..' / args.solution.stem).resolve()
    savedir.mkdir(parents=True, exist_ok=True)

    for i, solution in enumerate(solutions.solutions):
        path_out = savedir / (args.solution.stem + ('_%02d.pdf' % i))
        save_solution(problem, solution,
                      filename_out=str(path_out),
                      textsize=args.textsize,
                      show_flow=args.show_flow,
                      show_axes=args.show_axes,
                      show_fitness=args.show_fitness)

    intra_path = savedir / (args.solution.stem + '_intra.pdf')
    inter_path = savedir / (args.solution.stem + '_inter.pdf')
    plot_intra_island_diversity(solutions.intra_diversity, str(intra_path))
    plot_inter_island_diversity(solutions.inter_diversity, str(inter_path))
