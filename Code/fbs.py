"""
Data structure containing the data and operations on FBS individuals
"""

import random

import deap
import deap.tools
import deap.base

import numpy as np

import geometry


class FBS(object):
    """
    Create, modify, mutate, crossover and print facilities individuals
    """
    def __init__(self, fitness, n_dep):
        """
        Initialize a FBS individual with
        fac - Facility indexes in random order [n]
        """
        self.fac = list(range(n_dep))
        random.shuffle(self.fac)

        self.cut = [random.choice([0, 1]) for _ in range(n_dep)]

        self.fitness = fitness()

    def __str__(self):
        """
        Allows printing the facility genotype
        """
        return str( (self.fac, self.cut) )

    def mutate(self):
        """
        Mutate individuals deparment order or cut points with half probability
        """
        if random.random() < 0.5:
            # Swap two departments
            order = self.fac
            i, j = random.sample(range(len(order)), 2)
            order[i], order[j] = order[j], order[i]
        else:
            cut_points = self.cut
            size = len(cut_points)
            if random.random() < 0.5:
                # Flip cut point
                i = random.randrange(size)
                cut_points[i] = 0 if (cut_points[i] == 1) else 1
            else:
                # Shift cut point
                size -= 1 # Ignore orientation bit
                pos = random.randrange(size)
                for i in range(size):
                    if cut_points[(pos+i) % size] == 1:
                        break
                pos = (pos+i) % size
                cut_points[pos] = 0
                if random.random() < 0.5:
                    cut_points[(pos+1) % size] = 1
                else:
                    cut_points[(pos-1) % size] = 1

    def crossover(self, indiv):
        """
        Apply genetic operator crossover
        """
        deap.tools.cxPartialyMatched(self.fac, indiv.fac)
        deap.tools.cxOnePoint(self.cut, indiv.cut)

def split_bays(order, cut_points):
    """
    Find bays (facilities for each bay)

    Return: [ [first_fac_index_in_bay1, second_fac_index_in_bay1, ...],
              [first_fac_index_in_bay2, ...],
              ... ]
    """
    n_fac = len(order)
    bays = []
    bay = [order[0]]
    for i in range(n_fac-1):
        if cut_points[i] == 1:
            bays.append(bay)
            bay = []
        bay.append(order[i+1])
    bays.append(bay)

    return bays

def facilities_coordinates(individual, problem):
    """
    Calculate the coordinates of the facilities in a FBS (Flexible Bay Structure)
        order: list with the order of the n facilities identified by index
        cut_points: list with n-1 {0,1} values identifying cut point with value 1
        problem: fldata.Problem
    Return: [(XtlF0, YtlF0, XbrF0, YbrF0), (XtlF1, ...), ... ]
    """
    order, cut_points = individual
    width, height = problem.get_one_floor_rectangle()
    n_fac = len(order)
    bays = split_bays(order, cut_points)

    # Area needed for each bay
    bay_areas = []
    for bay in bays:
        bay_areas.append( sum([problem.departments[i][2] for i in bay]) )

    # Coordinates of each facility
    coordinates = [None] * n_fac
    bay_index = 0
    # - Horizontal bays
    if len(cut_points) == n_fac and cut_points[-1]:
        bottom_border = 0.0
        for bay in bays:
            bay_height = bay_areas[bay_index] / float(width)
            top_border = bottom_border + bay_height

            left_border = 0.0 # Fill bays in the sense of x axis increment
            for facility in bay:
                facility_width = problem.departments[facility][2] / bay_height
                right_border = left_border + facility_width
                coordinates[facility] = (left_border, top_border, right_border, bottom_border)
                left_border = right_border

            bottom_border = top_border
            bay_index += 1

    # - Vertical bays
    else:
        left_border = 0.0
        for bay in bays:
            bay_width = bay_areas[bay_index] / float(height)
            right_border = left_border + bay_width

            bottom_border = 0.0 # Fill bays in the sense of y axis increment
            for facility in bay:
                facility_height = problem.departments[facility][2] / bay_width
                top_border = bottom_border + facility_height
                coordinates[facility] = (left_border, top_border, right_border, bottom_border)
                bottom_border = top_border

            left_border = right_border
            bay_index += 1

    return coordinates


def penalty(facilities, coord):
    """
    Penalty function from Tate&Smith 1995 with k=1 (number of unfeasible departments)
    (adaptation to objective values in fitness function)

    facilities = problem.departments (as in data.py)
    coord = coordinates of each facility
            [(XtlF0, YtlF0, XbrF0, YbrF0), (XtlF1, ...), ... ]
    """
    coef = 0
    n_facilities = len(facilities)
    for fac in range(n_facilities):
        width = abs(coord[fac][0] - coord[fac][2])
        height = abs(coord[fac][1] - coord[fac][3])
        ratio = max(width, height) / min(width, height)
        if ratio > facilities[fac].ar_max:
            coef += 1
        #print facilities[fac].name, str(width) + 'x' + str(height), ratio, coef
    return coef

class FlowWithAdaptativePenaltyEvaluator(object):
    """
    Evaluator of material flow with adaptative penalty from Tate&Smith
    """
    def __init__(self, problem_data, facilities_coordinates_calculator=facilities_coordinates):
        self.problem = problem_data
        if problem_data.distance == 'Euclidean':
            self.distance = geometry.euclidean_distance
        elif problem_data.distance == 'Manhattan':
            self.distance = geometry.manhattan_distance
        elif problem_data.distance == 'Chebyshev':
            self.distance = geometry.cranes_distance
        else:
            raise Exception('Unsupported distance: ' + problem_data.distance)
        self.facilities_coordinates_calculator = facilities_coordinates_calculator
        self.best_overall_flow = None
        self.best_feasible_flow = None


    def __call__(self, individual):
        """
        Individual evaluation with two objectives:
          - Material flow
          - User model evaluation
          Increments material flow with penalty as Tate&Smith

        individual, the GA individual as defined in init_fbs_individual
        """
        n_fac = len(self.problem.departments)

        # Evaluate material flow
        coord = self.facilities_coordinates_calculator(individual, self.problem)
        centers = geometry.rectangles_to_centers(coord)

        flow_matrix = self.problem.tables['CostFlow']
        material_flow = 0.0
        for i in range(n_fac):
            for j in range(n_fac):
                material_flow += flow_matrix[i][j] * self.distance(centers[i], centers[j])

        # Aspect ratio penalty
        penalty_coef = penalty(self.problem.departments, coord)

        #   Update best records
        if self.best_overall_flow is None or self.best_overall_flow > material_flow:
            self.best_overall_flow = material_flow
        if penalty_coef == 0 and (self.best_feasible_flow is None or
                                  self.best_feasible_flow > material_flow):
            self.best_feasible_flow = material_flow

        #   Calculate adaptation
        if self.best_feasible_flow is None:
            adaptation = self.best_overall_flow
        else:
            adaptation = self.best_feasible_flow - self.best_overall_flow

        return (material_flow + penalty_coef * adaptation, )

class FlowEvaluator(object):
    """
    Evaluator of material flow
    """
    def __init__(self, problem_data, facilities_coordinates_calculator=facilities_coordinates):
        self.problem = problem_data
        if problem_data.distance == 'Euclidean':
            self.distance = geometry.euclidean_distance
        elif problem_data.distance == 'Manhattan':
            self.distance = geometry.manhattan_distance
        elif problem_data.distance == 'Chebyshev':
            self.distance = geometry.cranes_distance
        else:
            raise Exception('Unsupported distance: ' + problem_data.distance)
        self.facilities_coordinates_calculator = facilities_coordinates_calculator


    def __call__(self, individual):
        """
        Individual evaluation with the objective of minimizing material flow.

        individual, the GA individual as defined in init_fbs_individual
        """
        n_fac = len(self.problem.departments)

        # Evaluate material flow
        coord = self.facilities_coordinates_calculator(individual, self.problem)
        centers = geometry.rectangles_to_centers(coord)

        flow_matrix = self.problem.tables['CostFlow']

        material_flow = 0.0
        for i in range(n_fac):
            for j in range(n_fac):
                material_flow += flow_matrix[i][j] * self.distance(centers[i], centers[j])

        return (material_flow,)
