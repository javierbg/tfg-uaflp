"""
Handle **facility layout data**: *Problem*, *Parameters* and *Solutions*. The usual data flow in the
system using this entities is:
"""
import collections
import ast
import os
import os.path
import sys
import datetime
import socket
import optparse
import logging
import math
import itertools
import pprint

Department = collections.namedtuple('Department', ['name', 'type', 'area', 'min_side',
                                               'ar_min', 'ar_opt_start', 'ar_opt_end', 'ar_max'])

class DataMaster(object):
    """
    Master class for all data classes (Problem, Parameters, Solutions). These classes can store,
    display, load and save the data from its defined structure. The information stored can be
    represented as a string with python code with the same structure.

    All subclasses must implement:
        * __repr__, returning a string with the data structure.
        * build, loading the data from the structure string.

    All subclasses can use generic load and save methods defined here.
    """
    def load(self, data_file):
        """
        Load the information from opened data_file
        """
        #Interpretation and loading
        try:
            doc = data_file.read()
            structure = ast.literal_eval(doc)
            self.build(structure)
        except ValueError:
            print("Error reading file:", data_file)
            raise

    def save(self, filename):
        """
        Store the data structure in filename (overwrite file if exists)
        """
        f2save = open(filename, 'w')
        f2save.write(str(self))
        f2save.close()

    def build(self, structure):
        raise NotImplementedError()

class Problem(DataMaster):
    """
    Facility Layout problem. This is a static representation of all the problem data (it is not
    expected to change, it does not include links to solutions)
    """

    def __init__(self, filename=None, structure=None, fileopen=None, problem_name=None):
        """
        Initializes a problem: empty, with the data loaded
        from filename, or with data loaded from structure
        """
        self.problemname = problem_name
        self.description = None
        self.distance = 'Euclidean'
        self.floors = ()
        self.departments = []
        self.tables = {}

        self.dep = {} # Fast access to departments by name
        self.max_cost_flow = None

        # Load from file
        if filename:
            self.problemname, _ = os.path.splitext( os.path.basename(filename) )
            try:
                f = open(filename)
                self.load(f)
            except IOError:
                raise Exception("Error loading problem file:" + filename)
            finally:
                f.close()

        # Load from structure
        elif structure and problem_name:
            self.build(structure)

        # Load from an already opened file (django)
        elif fileopen and problem_name:
            try:
                self.load(fileopen)
            except IOError:
                raise Exception("Error loading problem file:" + filename)


    # Tool functions to easy access data
    def get_required_area(self):
        """
        Return the sum of the area required for all facilities
        """
        req_area = 0.0
        for department in self.departments:
            req_area += department.area
        return req_area


    def get_one_floor_rectangle(self):
        """
        Get the rectangular size of the only plant

        If more than one plant is defined raise Exception
        If non rectangular plant, ignores and return the rectangle in which it is inscribed

        Return: (x_width, y_heigh)
        """
        if len(self.floors) < 1:
            raise Exception('No floors defined')
        if len(self.floors) > 1:
            raise Exception('More than one floor defined. Unsupported by this algorithm.')

        polygon = self.floors[0][1]
        x_coord, y_coord = list(zip(*polygon))
        xmin, ymin = min(x_coord), min(y_coord)
        xmax, ymax = max(x_coord), max(y_coord)
        return ( float(xmax-xmin), float(ymax-ymin) )

    def build(self, structure):
        """
        Insert data in object from problem pydata structure
        """
        if len(structure) < 5:
            raise Exception('Few main data in problem file, need: description, distance, floors, departments, tables (maybe distance is not included in an old problem)')

        self.description, self.distance, self.floors, departments, tables = structure
        self.tables = dict(tables)

        # Check distance
        if self.distance not in ['Euclidean', 'Manhattan', 'Chebyshev']:
            raise Exception('Unsupported distance (%s)' % (self.distance,))

        # Build department objects and the fast department index
        for department_tuple in departments:
            department = Department._make(department_tuple)
            self.departments.append(department)
            self.dep[department[0]] = department

        # Check size of exteriors is appropiate
        for floor in self.floors:
            level, polygon, exteriors = floor
            if exteriors and len(exteriors) != len(polygon):
                raise Exception('On level %d, no. exteriors (%d) not equal to no. edges (%d)' % (level, len(exteriors), len(polygon)) )

        # Create virtual CostFlow table by scalar multication of Flow matrix by Cost (if exist)
        if 'Cost' in self.tables:
            # Preconditions: if 'Cost' then 'Flow' must be defined and be of the same size
            flow = self.tables['Flow']
            cost = self.tables['Cost']
            flow_cost = []
            for i, flow_row in enumerate(flow):
                row = []
                for j, value in enumerate(flow_row):
                    row.append(value * cost[i][j])
                flow_cost.append(row)
            self.tables['CostFlow'] = flow_cost
        elif 'Flow' in self.tables:
            self.tables['CostFlow'] = self.tables['Flow']

        # Precalculate max value of the flow between two departments (used often)
        if 'CostFlow' in self.tables:
            mcf_table = self.tables['CostFlow']
            self.max_cost_flow = 0.0
            for i in range(len(self.departments)):
                for j in range(i+1,len(self.departments)):
                    mcf = mcf_table[i][j] + mcf_table[j][i]
                    if mcf > self.max_cost_flow:
                        self.max_cost_flow = mcf


    def __repr__(self):
        """
        Represents the data structure of the problem through a string
        """
        # Description and distance
        str_final  = '("""' + self.description + '""",\n\'' + self.distance + "',\n"

        # Floors
        str_final += "\n # List of plants:\n"
        str_final += " (\t#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])\n"
        for floor in self.floors:
            str_final += "\t" + str(floor) + ",\n"
        str_final += " ),\n"

        # Facilities
        str_final += "\n # List of departments:\n"
        str_final += " (\t#(Name,      Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)\n"
        for department in self.departments:
            name, f_type, m_area, m_side, ar1, ar2, ar3, ar4 = department
            str_final += '\t(%-10s, %-10s, %8s, %8s, %6s, %6s, %6s, %6s),\n' % ("'" + name + "'",
                         "'" + f_type + "'", str(m_area), str(m_side),
                         str(ar1), str(ar2), str(ar3), str(ar4) )

#            str_final += "\t" + str(department) + ",\n"
        str_final += " ),\n"

        # Relation tables
        str_final += "\n # Dictionary of relation tables:\n"
        str_final += " {\t# Table name : [Data rows]\n"
        table_names = sorted(self.tables.keys())
        for table_name in table_names:
            if table_name != 'CostFlow': # Avoid storing virtual table
                str_final += "\t'" + table_name + "': [\n"
                tab = ' ' * (len(table_name) + 7)
                table = self.tables[table_name]
                for i, row in enumerate(table):
                    str_final += tab + str(row) + ", #%d\n" % (i,)
                str_final += '\t],\n'
        str_final += " }\n"

        str_final += ")\n"

        return str_final


class Parameters(DataMaster):
    """
    Parameter values to run an algorithm.
    """
    def __init__(self, filename=None, structure=None):
        """
        Initializes the parameters, empty or with the data loaded from filename
        """
        #Variables of parameters problems
        self.algorithmname = None
        self.historic = None
        self.parameters = collections.OrderedDict()
        self.init_individuals = []

        # Load from file
        if filename:
            try:
                f = open(filename)
                self.load(f)
            except IOError:
                print("Error opening problem file", filename)
                raise
            finally:
                f.close()
        # Load from structure
        elif structure:
            self.build(structure)

    def build(self, structure):
        """
        Load parameter data from the structure
        """
        if len(structure) == 4:
            self.algorithmname = structure[0]
            self.historic = structure[1]
            self.parameters = structure[2]
            self.init_individuals = structure[3]
        else:
            raise Exception('Parameter format unknown')

    def __repr__(self):
        """
        Represents the data structure of the problem through a string
        """
        # Name, description and history boolean
        str_final  = "(" + self.algorithmname.__repr__() + ",\n"
        str_final += str(self.historic) + ",\n"

        # Each parameter on one line
        str_final += " {\n"
        for param, value in list(self.parameters.items()):
            str_final += "\t" + param.__repr__() + " : " + value.__repr__() + ",\n"
        str_final += " },\n"

        # Each init individual on one line
        str_final += " (\n"
        for individual in self.init_individuals:
            str_final += "\t" + individual + ",\n"
        str_final += " ),\n"

        str_final += ")\n"

        #Return the final string
        return str_final


class Solutions(DataMaster):
    """
    A set of solutions to a facility layout problem. Includes information about what algorithm was
    run, with which parameters, when, how long it took.
    """
    def __init__(self, filename=None, problemname=None, structure=None, parameters=None, cpu_time=-1, intra_diversity=None, inter_diversity=None, migration_matrix=None):
        """
        Initialize a solution, empty or with the data loaded from filename
        """
        self.problemname = problemname
        if parameters:
            self.parameters = parameters
        else:
            self.parameters = Parameters() # Empty parameters, using None may give problems
        self.fitness_header = []
        self.solutions = set()
        self.datetime = str(datetime.datetime.today())
        self.server = socket.gethostname()
        self.processor = _getProcessorModel()
        self.cpu_time = cpu_time
        self.intra_diversity = intra_diversity
        self.inter_diversity = inter_diversity
        self.migration_matrix = migration_matrix

        if structure:
            self.build(structure)
        elif filename:
            try:
                self.load( open(filename) )
            except IOError:
                print("Error to open the solutions file", filename)
                raise

    def build(self, structure):
        """
        Load solutions from structure
        """
        try:
            if len(structure) < 8:
                raise Exception("Invalid solutions structure format (missing data). New version include problemname and 4 fields of run data.")
        except TypeError:
            raise Exception("Invalid solutions structure format")

        self.problemname = structure[0]
        self.parameters = Parameters(structure=structure[1])
        self.fitness_header = structure[2]
        self.solutions = structure[3]
        self.datetime = structure[4]
        self.server = structure[5]
        self.processor = structure[6]
        self.cpu_time = structure[7]
        self.intra_diversity = structure[8]
        self.inter_diversity = structure[9]
        try:
            self.migration_matrix = structure[10]
        except IndexError:
            pass # No se ha guardado matriz de migración


    def __repr__(self):
        """
        Represents the data structure of the problem through a string
        """
        no_fitnesses = len(self.fitness_header)
        assert all(len(s[1]) == no_fitnesses for s in self.solutions), \
               'Invalid number of fitness values'
        assert self.problemname is not None, \
               'The field problemname must be filled'

        data_out = (
            self.problemname,
            self.parameters,
            self.fitness_header,
            list(self.solutions),
            self.datetime,
            self.server,
            self.processor,
            self.cpu_time,
            self.intra_diversity,
            self.inter_diversity,
            self.migration_matrix.tolist() if self.migration_matrix is not None else None,
        )

        return pprint.pformat(data_out, indent=4)

def _getProcessorModel():
    """
    Get processor model

    returns: a string with processor model or 'Undefined'
    """
    processor_model = 'Undefined'
    try:
        cpuinfo_file = open('/proc/cpuinfo')
        data = cpuinfo_file.readlines()
        cpuinfo_file.close()
        for line in data:
            if line.find('model name') != -1:
                processor_model = line[line.find(':')+2:-1]
    except IOError:
        pass

    return processor_model

def new_solutions_filename(problem_name, parameters_filename):
    """
    Find a new correlative solutions filename
    """
    i = 0
    pathfile, ext = os.path.splitext(parameters_filename)
    structure = 'sol_%s_%s' % (os.path.basename(pathfile), problem_name)
    sol_filename = structure + '%02d.py' % (i, )
    while os.path.exists(sol_filename):
        i += 1
        sol_filename = structure + '%02d.py' % (i, )

    return sol_filename

def dummy_progress_callback(progress):
    """
    Progress callback function that does nothing
    """
    pass

def print_progress_callback(progress):
    """
    Progress callback function that just print progress on stdout
    """
    print('Progress:', progress * 100)

def max_problem_distance(problem):
    """
    Computes the distance between the most distant vertexes in a problem.
    Assumes single floor
    """
    floor_vertexes = problem.floors[0][1]
    distances = [math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2) for p1, p2 in itertools.combinations(floor_vertexes, 2)]
    return max(distances)*len(problem.departments)

def alg_main_params(alg, problem, parameters, sol_filename):
    # Logging system
    logger = logging.getLogger('ga_test')
    logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    # Run algorithm
    problem.max_distance = max_problem_distance(problem)
    solutions = alg.run(problem, parameters, dummy_progress_callback, logger)

    # Save results and exit
    solutions.save(sol_filename)
    return solutions

def alg_main(alg, print_progress=True):
    """
    A main function to run the algorithm locally with files from command line
    """
    # Parse arguments and options
    usage = "\n  %prog [options] problem parameters"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-g", "--generate",
                      action="store_true", dest="generate", default=False,
                      help="generate an example parameters file with default values")
    (options, args) = parser.parse_args()

    if len(args) < 2:
        parser.error("problem and parameters must be specified")

    else:
        problem_filename = args[0]
        parameters_filename = args[1]

    problem = Problem(problem_filename)
    parameters = Parameters(parameters_filename)
    sol_filename = new_solutions_filename(problem.problemname, parameters_filename)

    return alg_main_params(alg, problem, parameters, sol_filename)
