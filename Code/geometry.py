"""
Geometry module
"""
import math

def euclidean_distance(point_a, point_b):
    """
    Euclidean distance from point_a to point_b

    point_a, tuple with two dimensional point coordinates (a_x, a_y)
    point_b, idem (b_x, b_y)
    """
    return math.sqrt((point_b[0] - point_a[0])**2 + (point_b[1] - point_a[1])**2)

def manhattan_distance(point_a, point_b):
    """
    Manhattan distance from point_a to point_b
    """
    return math.fabs(point_b[0] - point_a[0]) + math.fabs(point_b[1] - point_a[1])

def cranes_distance(point_a, point_b):
    """
    Cranes distance from point_a to point_b
    """
    return max(math.fabs(point_b[0] - point_a[0]) , math.fabs(point_b[1] - point_a[1]))

def rectangles_to_centers(coordinates):
    """
    Given a list of the rectangular coordinates (top left and bottom right corners) for a series of
    facilities (F0, F1, ...):
      [(XtlF0, YtlF0, XbrF0, YbrF0), (XtlF1, ...), ... ]

    return a list with the center of each facility:
      [(XcF0, YcF0), (XcF1, YcF1), ... ]
    """
    return [((xbr+xtl)/2.0, (ybr+ytl)/2.0) for xtl, ytl, xbr, ybr in coordinates]
