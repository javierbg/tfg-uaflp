'''
Worker islands code
'''

import time
import random
import operator

import misc


class WorkerIsland:
    def __init__(self, identifier, params, msgQueue, managerPipe, toolbox, logger):
        self.id = identifier
        self.msgQueue = msgQueue
        self.managerPipe = managerPipe
        self.params = params
        self.tb = toolbox
        self.evaluator = toolbox.get_evaluator()
        self.logger = logger

    def evaluate_population(self, pop):
        for ind in pop:
            ind.fitness.values = self.evaluator((ind.fac, ind.cut))

    def generation(self, population):
        '''
        For each pair of parents It applies crossover and mutate operators
        '''
        offspring = [self.tb.clone(ind) for ind in population]

        # Apply crossover on the offspring
        for ind1, ind2 in zip(offspring[::2], offspring[1::2]):
            if random.random() < self.params['crossover_prob']:
                ind1.crossover(ind2)
                del ind1.fitness.values, ind2.fitness.values

        # Apply mutate on the offspring
        for ind in offspring:
            if random.random() < self.params['mutation_prob']:
                ind.mutate()
                del ind.fitness.values

        return offspring

    def run(self):
        # Initial population
        pop = self.tb.population(n=self.params['population_size_island'])
        self.evaluate_population(pop)

        ngen = self.params['number_of_generations']
        ngen_migrate = self.params['number_of_generations_migrate']

        work_time = comm_time = 0.0

        end_cycle = time.time()
        ngen_remaining = ngen
        while ngen_remaining > 0:
            ngen_next_cycle = min(ngen_remaining, ngen_migrate)
            ngen_remaining -= ngen_next_cycle

            start_cycle = end_cycle
            for _ in range(ngen_next_cycle):
                # Generation
                # Select the next generation individuals
                offspring = self.tb.select(pop, len(pop))

                # Variate the pool of individuals
                offspring = self.generation(offspring)

                # Evaluate the individuals with an invalid fitness
                invalid_ind = (ind for ind in offspring if not ind.fitness.valid)
                self.evaluate_population(invalid_ind)

                # Replace the current population with the offspring
                pop = offspring

            # Sort by fitness (best first)
            pop = sorted(pop, key=operator.attrgetter('fitness'), reverse=True)

            # Migrate best
            emigrant = pop[0]
            diversity = self.tb.diversity(pop)

            start_comm = time.time()

            self.msgQueue.put({
                'sender': self.id,
                'message': 'migrant',
                'migrant': emigrant,
                'diversity': diversity,
            })

            response = self.managerPipe.recv()
            if response['message'] == 'migrant':
                pop[0] = response['migrant']

            end_cycle = time.time()

            work_time += start_comm - start_cycle
            comm_time += end_cycle - start_comm

        self.logger.info("Worker %d work time: %f %%" % (self.id, (work_time*100) / (work_time + comm_time)))
        self.msgQueue.put({
            'sender': self.id,
            'message': 'finished',
        })
