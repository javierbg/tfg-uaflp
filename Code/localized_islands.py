#!/usr/bin/env python3

from lsa import LocalizationStrategyAlgorithm
import fldata

fldata.alg_main(LocalizationStrategyAlgorithm(), print_progress=False)
