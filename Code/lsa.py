#!/usr/bin/env python3

import time
import multiprocessing
import numpy as np
import os
import random
import itertools
import copy

import deap
import deap.base
import deap.creator
import deap.tools

import fldata
import fbs
from islands import WorkerIsland
import misc


class LocalizationStrategyAlgorithm:
    '''
    Genetic Coevolutive Cooperative Algorithm using deap to solve flexible bay structure problem
    '''

    @classmethod
    def run(cls, problem, parameters, progress_callback, logger):
        '''
        Function that runs the complete genetic algorithm

        problem = object with problem (fldata.Problem)
        parameters = object with parameters (fldata.Parameters)
        progress_callback = a function to call with progress as a float in [0,1]
        or -1 in case of failure
        logger = a logging.Logger object for all messages to print

        return a fldata.Solution object
        '''
        # Start
        alg_name = parameters.algorithmname
        problem_name = problem.problemname
        logger.info('Start ' + alg_name + ' on ' + problem_name)

        num_facilities = len(problem.departments)

        toolbox = deap.base.Toolbox()

        # Define deap individual
        deap.creator.create('fitness', deap.base.Fitness, weights=(-1.0, ) )

        # Create population
        toolbox.register('individual', fbs.FBS, fitness=deap.creator.fitness, n_dep=num_facilities)
        toolbox.register('population', deap.tools.initRepeat, list, toolbox.individual)

        # Define the evaluator and selection operators
        toolbox.register('get_evaluator', fbs.FlowWithAdaptativePenaltyEvaluator, problem)
        toolbox.register('select', deap.tools.selTournament, tournsize=2)
        toolbox.register('distance', misc.distance, problem=problem)
        toolbox.register('diversity', misc.diversity, problem=problem)

        # Define needed parameters
        diversity_threshold = parameters.parameters['diversity_threshold']
        n_workers = parameters.parameters['n_workers']

        # Generate random params for workers, using limits provided
        ranges = parameters.parameters['workers_params_range']
        worker_params = list()
        for _ in range(n_workers):
            worker_params.append({
                'number_of_generations' : random.randint(ranges['number_of_generations'][0], ranges['number_of_generations'][1]),
                'number_of_generations_migrate' : random.randint(ranges['number_of_generations_migrate'][0], ranges['number_of_generations_migrate'][1]),
                'population_size_island' : random.randint(ranges['population_size_island'][0], ranges['population_size_island'][1]),
                'mutation_prob' : random.uniform(ranges['mutation_prob'][0], ranges['mutation_prob'][1]),
                'crossover_prob' : random.uniform(ranges['crossover_prob'][0], ranges['crossover_prob'][1]),
            })
        parameters.parameters['workers_params'] = worker_params # Save the concrete params in the solution file

        # Build state parameters
        migrating = [None] * n_workers # Migrating individuals for each island
        last_migrating = [None] * n_workers
        running = [True] * n_workers # Wether island i is running

        # Queue for communications toward this process
        qu = multiprocessing.SimpleQueue()
        # Build pipes for sending info back to the islands (one-way only)
        recvPipes, sendPipes = zip(*[multiprocessing.Pipe(duplex=False) for _ in range(n_workers)])

        # Hall of fame to keep the best seen individuals
        halloffame = deap.tools.HallOfFame(n_workers * parameters.parameters['n_best_keep_per_island'])

        # Start stopwatch
        start = time.time()

        #Build and launch workers
        workers = [WorkerIsland(i, worker_params[i], qu, recvPipes[i], toolbox, logger) for i in range(n_workers)]
        workers_proc = [multiprocessing.Process(target=workers[i].run) for i in range(n_workers)]
        for wp in workers_proc:
            wp.start()

        diversity_history = [list() for _ in range(n_workers)]
        median_distances = list() #[(0, [None]*n_workers)]

        # Number of migrations between islands
        # Row: from, Column: to
        migrant_history = np.zeros(shape=(n_workers, n_workers), dtype=int)

        logger.info('Launched workers')

        while any(running):
            # Collect distance data between migrating individuals
            if all(last_migrating):
                mds = list()
                for i in range(n_workers):
                    this = last_migrating[i]
                    rest = last_migrating[:i] + last_migrating[i+1:]
                    mds.append(misc.distancia_mediana(this, rest, problem))
                median_distances.append((time.time() - start, mds))


            # Islands return a dictionary with the communication data
            response = qu.get()
            sender = response['sender']

            if response['message'] == 'migrant':
                available_migrant = any(migrating)
                migrating[sender] = last_migrating[sender] = response['migrant']
                halloffame.update([migrating[sender]])
                diversity_history[sender].append((time.time() - start, response['diversity']))
                if (response['diversity'] < diversity_threshold) and available_migrant:
                    sender_migrant = migrating[sender]

                    # Compute distances, ignoring itself
                    distances = [(toolbox.distance(sender_migrant, m) if m is not None else 0) for m in (migrating[:sender] + [None] + migrating[sender+1:])]
                    furthest_index = max(range(len(distances)), key=lambda i: distances[i])
                    furthest = migrating[furthest_index]
                    if furthest is not None and distances[furthest_index] > 0:
                        sendPipes[sender].send({
                            'message' : 'migrant',
                            'migrant' : furthest,
                        })
                        migrating[furthest_index] = None
                        migrant_history[furthest_index, sender] += 1
                    else:
                        sendPipes[sender].send({'message' : 'no_migrant'})

                else:
                    sendPipes[sender].send({'message' : 'no_migrant'})

            elif response['message'] == 'finished':
                running[sender] = False
                workers_proc[sender].join()

        # Return solution data
        # Get the most diverse within the hall of fame
        # Filter out those with invalid aspect ratio departments
        is_valid = lambda indv: fbs.penalty(problem.departments, fbs.facilities_coordinates((indv.fac, indv.cut), problem)) == 0

        hof_list = [indv for indv in halloffame if is_valid(indv)]
        med_distance = list()
        for i, indv in enumerate(hof_list):
            dist = misc.distancia_mediana(indv, hof_list[:i] + hof_list[i+1:], problem)
            med_distance.append((dist, indv))
        best_diverse = [i[1] for i in sorted(med_distance, key=lambda i: i[0])[:n_workers]]

        elapsed = time.time() - start

        logger.info('\n\nFINISHED\n\n')
        logger.info('Total time: %f secs' % elapsed)

        logger.info('Best fitness found: %f' % halloffame[0].fitness.values[0])

        return wrap_best_solutions(problem, parameters, elapsed, best_diverse, diversity_history, median_distances, migrant_history)

def wrap_best_solutions(problem, parameters, cpu_time, halloffame, intra_diversity, inter_diversity, migration_matrix):
    # Create solutions container
    solution = fldata.Solutions(problemname=problem.problemname,
                                parameters=parameters,
                                cpu_time=cpu_time,
                                intra_diversity=intra_diversity,
                                inter_diversity=inter_diversity,
                                migration_matrix=migration_matrix)
    solution.fitness_header = ['Material Flow']

    evaluator = fbs.FlowEvaluator(problem)
    # Save solutions from the hall of fame
    for indv in halloffame:
        sol_facilities = []
        coord = fbs.facilities_coordinates([indv.fac, indv.cut], problem)
        for i, facility in enumerate(problem.departments):
            fac_name = facility.name
            xtl, ytl, xbr, ybr = coord[i]
            polygon = ( (xtl, ytl), (xbr, ytl), (xbr, ybr), (xtl, ybr) )
            sol_facilities.append( (fac_name, 0, None, polygon) )

        # Include unique solutions
        sol = (-1, evaluator((indv.fac, indv.cut)), str( (indv.fac, indv.cut) ), None, tuple(sol_facilities))
        solution.solutions.add(sol)

    return solution
