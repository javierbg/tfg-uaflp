import numpy

import fbs
import geometry


def puntos180grados(puntos, problem):
    width, height = problem.get_one_floor_rectangle()
    return [(abs(p[0] - width), abs(p[1] - height)) for p in puntos]

def calcular_centros(indv, problem):
    centros = []
    coord = fbs.facilities_coordinates([indv.fac, indv.cut], problem)
    for i, facility in enumerate(problem.departments):
        fac_name = facility.name
        xtl, ytl, xbr, ybr = coord[i]
        centro_i = xbr - xtl, ytl - ybr
        centros.append( centro_i )
    return centros

# Calcula una medida de la diversidad de una población. Asume que está
# previamente ordenada por fitness.
def diversity(pop, problem):
    md = distancia_mediana(pop[0], pop[1:], problem)
    return md / problem.max_distance

# Distancia entre dos individuos
def distance(ind1, ind2, problem):
    dist = distancia_min(ind1, ind2, problem)
    return dist / problem.max_distance

# Devuelve la distancia mediana entre un individuo y un listado de individuos
def distancia_mediana(bestIndiv, indivs, problem):
    _distanceTemp = []
    for indiv in indivs:
        _distanceTemp.append(distancia_min(bestIndiv, indiv, problem))
    _mediana = numpy.median(_distanceTemp)
    return _mediana

# Calcula la distancia minima entre una planta principal y otra
# Se comprueba la distancia con la otra planta en su version original y 180 grados
def distancia_min(plantaPrincipal, planta2, problem):
    _cPlantaPrincipal = calcular_centros(plantaPrincipal, problem)
    _cPlanta2 = calcular_centros(planta2, problem)
    _d1 = numpy.abs(distancia_centros(_cPlantaPrincipal, _cPlanta2))
    _d2 = numpy.abs(distancia_centros(_cPlantaPrincipal, puntos180grados(_cPlanta2, problem)))
    return min(_d1,_d2)

def distancia_centros(centros1, centros2):
    return sum(geometry.euclidean_distance(c1, c2) for c1, c2 in zip(centros1, centros2))
