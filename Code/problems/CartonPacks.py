("""Encontrado/creado por Lorenzo para articulo Autosoft

Subjective criteria (para Expert Systems):
* A To be on the perimeter of the plant (next to the outside)
* F To be on the perimeter of the plant (next to the outside)
* D To be near of E, F and A
* C To be near of J, I, G and H
(Autosoft menciona tambien optimizar el flujo y los siguientes, que no son automatizables a priori):
* Whether the designer likes or not the placement of the remaining space.
* Whether he/she likes or not the distribution of spaces.
 """,
 'Euclidean',
 
 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0.0, 0.0), (20.0, 0.0), (20.0, 14.5), (0.0, 14.5)], []),
 ), # original height: 15, without empty space: 14.5

 # List of facilities:
 (	#(Name,      Type,              Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('A-Raw material',  'region'  ,       40,        0,      1,      1,      4,      4),
	('B-Finished prod.','region'  ,       40,        0,      1,      1,      4,      4),
	('C-Mechanic',      'region'  ,       20,        0,      1,      1,      4,      4),
	('D-Offices',       'region'  ,       50,        0,      1,      1,      4,      4),
	('E-Staff WC',      'region'  ,       20,        0,      1,      1,      4,      4),
	('F-Expedition',    'region'  ,       40,        0,      1,      1,      4,      4),
	('G-Hydraulic1',    'region'  ,       20,        0,      1,      1,      4,      4),
	('H-Hydraulic2',    'region'  ,       20,        0,      1,      1,      4,      4),
	('I-Crushing',      'region'  ,       20,        0,      1,      1,      4,      4),
	('J-Circ. saw',     'region'  ,       10,        0,      1,      1,      4,      4),
	('K-Heat exch.',    'region'  ,       10,        0,      1,      1,      4,      4),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Flow': [
	       #A, B, C, D, E, F, G, H, I, J, K
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0], #0 A
           [0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0], #1 B
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #2 C
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #3 D
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #4 E
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #5 F
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], #6 G
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1], #7 H
           [0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0], #8 I
           [0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0], #9 J
           [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0], #10 K
	],
	'Proximity': [
	       #A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #A
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #B
           [0,  0,  0,  0,  0,  0,  1,  1,  1,  1,  0], #C
           [1,  0,  0,  0,  1,  1,  0,  0,  0,  0,  0], #D
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #F
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #G
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #H
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #I
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #J
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #K
	],
 }
)
