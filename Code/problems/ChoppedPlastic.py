("""Encontrado/creado por Lorenzo para articulo WAC2012/Autosoft

Subjective criteria: 
* I must be near A, G and J due to process reasons.  
* K must be near C, D and E due to process reasons. 
* I must be far from C, D and E, due to noise.
 """,
 'Euclidean',

 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0.0, 0.0), (10.0, 0.0), (10.0, 30.0), (0.0, 30.0)], []),
 ),

 # List of facilities:
 (	#(Name,             Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('A-Reception',     'region'  ,       35,        0,      1,      1,      4,      4),
	('B-Raw material',  'region'  ,       50,        0,      1,      1,      4,      4),
	('C-Washing',       'region'  ,       15,        0,      1,      1,      4,      4),
	('D-Drying',        'region'  ,       24,        0,      1,      1,      4,      4),
	('E-Chopped',       'region'  ,       35,        0,      1,      1,      4,      4),
	('F-Finished prod.','region'  ,       30,        0,      1,      1,      4,      4),
	('G-Expedition',    'region'  ,       25,        0,      1,      1,      4,      4),
	('I-Office',        'region'  ,       30,        0,      1,      1,      4,      4),
	('J-Toilets',       'region'  ,       15,        0,      1,      1,      4,      4),
	('K-Repair shop',   'region'  ,       20,        0,      1,      1,      4,      4),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Flow': [
           #A, B, C, D, E, F, G,   I, J, K,  
           [0, 1, 0, 0, 0, 0, 0,   0, 0, 0], #A
           [0, 0, 1, 0, 0, 0, 0,   0, 0, 0], #B
           [0, 0, 0, 1, 0, 0, 0,   0, 0, 0], #C
           [0, 0, 0, 0, 1, 0, 0,   0, 0, 0], #D
           [0, 0, 0, 0, 0, 1, 0,   0, 0, 0], #E
           [0, 0, 0, 0, 0, 0, 1,   0, 0, 0], #F
           [0, 0, 0, 0, 0, 0, 0,   0, 0, 0], #G
           [0, 0, 0, 0, 0, 0, 0,   0, 0, 0], #I
           [0, 0, 0, 0, 0, 0, 0,   0, 0, 0], #J
           [0, 0, 0, 0, 0, 0, 0,   0, 0, 0], #K
	],
	'Proximity': [
           #A,  B,  C,  D,  E,  F,  G,    I,  J,  K,  
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #A
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #B
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #C
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #D
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #E
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #F
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #G
           [1,  0, -1, -1, -1,  0,  1,    0,  1,  0], #I
           [0,  0,  0,  0,  0,  0,  0,    0,  0,  0], #J
           [0,  0,  1,  1,  1,  0,  0,    0,  0,  0], #K
	],
}
)
