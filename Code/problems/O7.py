("""De Komarudin (2010), de Meller1999optimal 
    (en Meller no estan los datos completos, si verificado AR y manhattan distance)
    No se sabe de donde vienen la tabla Proximity

Komarudin, Kuan Yew Wong, Applying Ant System for solving Unequal Area Facility Layout Problems, European Journal of Operational Research, Volume 202, Issue 3, 1 May 2010, Pages 730-746, ISSN 0377-2217, http://dx.doi.org/10.1016/j.ejor.2009.06.016.
(http://www.sciencedirect.com/science/article/pii/S0377221709004834)

CORRECTO (flujo).
    
""",
 'Manhattan',
 
 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0, 0), (8.54, 0), (8.54, 13), (0, 13)], []),
 ),

 # List of facilities:
 (	#(Name,      Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('1'       , 'region'  ,       16,        0,    1.0,    1.0,    4.0,    4.0),
	('2'       , 'region'  ,       16,        0,    1.0,    1.0,    4.0,    4.0),
	('3'       , 'region'  ,       16,        0,    1.0,    1.0,    4.0,    4.0),
	('4'       , 'region'  ,       36,        0,    1.0,    1.0,    4.0,    4.0),
	('5'       , 'region'  ,        9,        0,    1.0,    1.0,    4.0,    4.0),
	('6'       , 'region'  ,        9,        0,    1.0,    1.0,    4.0,    4.0),
	('7'       , 'region'  ,        9,        0,    1.0,    1.0,    4.0,    4.0),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Flow': [
	       #1,#2,#3,#4,#5,#6,#7
           [0, 0, 0, 5, 0, 0, 1], #1
           [0, 0, 0, 3, 0, 0, 1], #2
           [0, 0, 0, 2, 0, 0, 1], #3
           [0, 0, 0, 0, 4, 4, 0], #4
           [0, 0, 0, 0, 0, 0, 2], #5
           [0, 0, 0, 0, 0, 0, 1], #6
           [0, 0, 0, 0, 0, 0, 0], #7
	],
	'Proximity': [
              [0, -8, 0, 4, 0, 0, 0], #0
              [0, 0, 0, 2, 0, 0, 0], #1
              [0, 0, 0, 1, 0, 0, 0], #2
              [0, 0, 0, 0, 3, 3, 0], #3
              [0, 0, 0, 0, -4, -4, 1], #4
              [0, 0, -4, 0, 0, 0, 0], #5
              [0, 0, 0, 0, 0, 0, 0], #6
	],
 }
)
