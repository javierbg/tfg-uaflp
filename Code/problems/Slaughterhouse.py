("""Problema del articulo: 
     * Salas-Morera, L., Cubero-Atienza, A., Ayuso-Munoz, R. Computer-aided plant layout. Volume 7, Issue 4, 1996, pp 39-46.
     * Descrito en original IGA (ASOC2013)
     
Criterios deseados por el usuario:
    1) A-Stables debe estar en una punta.
    2) A-Stables debe estar lejos de: E-camara de oreo, F-camara de refrigeracion e I-compresores.
    3) A-Stables, C-triperia, D-cuero y pieles, I-compresores, L-expedicion de subproducto y G-camara de tripas deben estar lejos de K-oficinas y J-shipping.
    4) J-Shipping y L-ByProduct shipping deben estar en el perimetro y muy lejanos entre si.

Para los flujos:
    De A a B el 100%.
    Por la linea B, E, F, J el 60%
    Por la linea B, C, G, L el 15%
    Por la linea B, D, L el 10%
    
  Nota: No coincide con exactamente con el articulo Salas1996.
  
  Usados con esta variante para la generacion de plantas para el articulo Neurocomputing con Cesar
  ('A-Stables',       'region'  ,      570,        0,      1,      1,      3,      4),#0
  ('B-Slaughter',     'region'  ,      206,        0,      1,      1,      4,      5),
  ('C-Entrails',      'region'  ,      150,        0,      1,      1,      4,      5),
  ('D-Leather & skin','region'  ,       55,        0,      1,      1,      4,      5),
  ('E-Aeration chamber', 'region',     114,        0,      1,      1,      4,      5),
  ('F-Refrigeration chamber', 'region',102,        0,      1,      1,      2,      4),#5
  ('G-Entrails chamber', 'region',      36,        0,      1,      1,      3,      4),
  ('H-Boiler room',   'region'  ,       26,        0,      1,      1,      3,      4),
  ('I-Compressor room', 'region',       46,        0,      1,      1,      3,      4),
  ('J-Shipping',      'region'  ,      109,        0,      1,      1,      3,      4),
  ('K-Offices',       'region'  ,       80,        0,      1,      1,      3,      4),#10
  ('L-Byproduct shiping', 'region',     40,        0,      1,      1,      3,      4),
  ('E1',              'empty',         40,        0,      1,      1,      100000, 100000),
  ('E2',              'empty',         30,        0,      1,      1,      100000, 100000),
  ('E3',              'empty',         20,        0,      1,      1,      100000, 100000),
  ('E4',              'empty',         15,        0,      1,      1,      100000, 100000),
  ('E5',              'empty',          8,        0,      1,      1,      100000, 100000),
  ('E6',              'empty',          3,        0,      1,      1,      100000, 100000),
 """,
 'Euclidean',


 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0.0, 0.0), (51.14, 0.0), (51.14, 30.0), (0.0, 30.0)], ['bottom', 'right', 'top', 'left']),
 ),# Largo era 55.0, 51.14 sin espacio vacio

 # List of facilities:
 (	#(Name,             Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('A-Stables',       'region'  ,      570,        0,      1,      1,      4,      4),#0
	('B-Slaughter',     'region'  ,      206,        0,      1,      1,      4,      4),
	('C-Entrails',      'region'  ,      150,        0,      1,      1,      4,      4),
	('D-Leather & skin','region'  ,       55,        0,      1,      1,      4,      4),
	('E-Aeration chamber', 'region',     114,        0,      1,      1,      4,      4),
	('F-Refrigeration chamber', 'region',102,        0,      1,      1,      4,      4),#5
	('G-Entrails chamber', 'region',      36,        0,      1,      1,      4,      4),
	('H-Boiler room',   'region'  ,       26,        0,      1,      1,      4,      4),
	('I-Compressor room', 'region',       46,        0,      1,      1,      4,      4),
	('J-Shipping',      'region'  ,      109,        0,      1,      1,      4,      4),
	('K-Offices',       'region'  ,       80,        0,      1,      1,      4,      4),#10
	('L-Byproduct shipping', 'region',     40,        0,      1,      1,      4,      4),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Adjacency': [
	       #A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  E1, E2, E3, E4, E5, E6
           [0,  0,  0,  0, -1, -1,  0,  0, -1,  0,  0,  0,  0,  0,  0,  0,  0,  0], #A
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #B
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #C
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #D
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #F
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #G
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #H
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #I
           [-1, 0, -1, -1,  0,  0, -1,  0, -1,  0,  0, -1,  0,  0,  0,  0,  0,  0], #J
           [-1, 0, -1, -1,  0,  0, -1,  0, -1,  0,  0, -1,  0,  0,  0,  0,  0,  0], #K
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #L
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E1
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E2
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E3
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E4
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E5
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E6
	],
	'Flow': [
	       #A,  B,  C,  D,  E,  F,  G,  H,  I,  J,  K,  L,  E1, E2, E3, E4, E5, E6
           [0,100,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #A
           [0,  0, 15, 10, 60,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #B
           [0,  0,  0,  0,  0,  0, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #C
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10,  0,  0,  0,  0,  0,  0], #D
           [0,  0,  0,  0,  0, 60,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E
           [0,  0,  0,  0,  0,  0,  0,  0,  0, 60,  0,  0,  0,  0,  0,  0,  0,  0], #F
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 15,  0,  0,  0,  0,  0,  0], #G
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #H
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #I
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #J
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #K
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #L
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E1
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E2
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E3
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E4
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E5
           [0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0], #E6
	],
 }
)
