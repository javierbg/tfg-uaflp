("""Test problem
  """,
 'Manhattan',

 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0, 0), (0, 2), (3, 2), (3, 0)], []),
 ),

 # List of facilities:
 (	#(Name,      Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('1'       , 'region'  ,      1,         0,      1,    0.7,    0.7,    4),
	('2'       , 'region'  ,      1,         0,      1,      1,      1,    4),
	('3'       , 'region'  ,      2,         0,      1,    0.7,    0.7,    4),
	('4'       , 'region'  ,      2,         0,      1,    0.5,    0.5,    4),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Flow': [
           [0, 1, 1, 1], #0
           [1, 0, 1, 1], #1
           [1, 1, 0, 1], #2
           [1, 1, 0, 1], #3
           [1, 1, 1, 0], #4
	],
 }
)
