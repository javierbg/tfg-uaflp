("""Tomado de Komarudin (2010), de van Camp (1991)
     (van Camp habla de exterior en flujo pero no parece usarlo, lado minimo y distancia euclidea confirmados)
 """,
 'Euclidean',
 
 # List of plants:
 (	#(level, [(x,y) coordinates of poligon vertexes], [Exterior type of every side])
	(1, [(0, 0), (25, 0), (25, 51), (0, 51)], []),
 ),

 # List of facilities:
 (	#(Name,      Type,       Min.Area, Min.Side,  ARMin, AROpt1, AROpt2,  ARMax)
	('1'       , 'region'  ,      238,        5,      1,      1,   9.52,   9.52),
	('2'       , 'region'  ,      112,        5,      1,      1,   4.48,   4.48),
	('3'       , 'region'  ,      160,        5,      1,      1,    6.4,    6.4),
	('4'       , 'region'  ,       80,        5,      1,      1,    3.2,    3.2),
	('5'       , 'region'  ,      120,        5,      1,      1,    4.8,    4.8),
	('6'       , 'region'  ,       80,        5,      1,      1,    3.2,    3.2),
	('7'       , 'region'  ,       60,        5,      1,      1,    2.4,    2.4),
	('8'       , 'region'  ,       85,        5,      1,      1,    3.4,    3.4),
	('9'       , 'region'  ,      221,        5,      1,      1,   8.84,   8.84),
	('10'      , 'region'  ,      119,        5,      1,      1,   4.76,   4.76),
 ),

 # Dictionary of relation tables:
 {	# Table name : [Data rows]
	'Flow': [
           [0, 0, 0, 0, 0, 218, 0, 0, 0, 0], #0
           [0, 0, 0, 0, 0, 148, 0, 0, 296, 0], #1
           [0, 0, 0, 28, 70, 0, 0, 0, 0, 0], #2
           [0, 0, 0, 0, 0, 28, 70, 140, 0, 0], #3
           [0, 0, 0, 0, 0, 0, 0, 210, 0, 0], #4
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #5
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 28], #6
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 888], #7
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 59], #8
           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0], #9
	],
 }
)
