#!/usr/bin/env python3

import unittest
import random
import multiprocessing
import math

import deap
import deap.base
import deap.creator
import deap.tools

import fbs
import fldata
import misc
from islands import WorkerIsland
from lsa import LocalizationStrategyAlgorithm


class UAFLPTestCase(unittest.TestCase):
    def setUp(self):
        self.problem = fldata.Problem(filename='problems/test_problem.py')
        self.n_dep = len(self.problem.departments)
        self.problem.max_distance = math.sqrt(13)*4

        self.toolbox = deap.base.Toolbox()
        deap.creator.create('fitness', deap.base.Fitness, weights=(-1.0, ) )
        self.toolbox.register('individual', fbs.FBS, fitness=deap.creator.fitness, n_dep=self.n_dep)
        self.toolbox.register('population', deap.tools.initRepeat, list, self.toolbox.individual)
        self.toolbox.register('evaluate', fbs.FlowEvaluator(self.problem))
        self.toolbox.register('select', deap.tools.selTournament, tournsize=2)
        self.toolbox.register('distance', misc.distance, problem=self.problem)
        self.toolbox.register('diversity', misc.diversity, problem=self.problem)

class FBSTestCase(UAFLPTestCase):
    def check_individual(self, individual):
        # Check department permutation size
        self.assertEqual(len(individual.fac), self.n_dep)

        # Check validity of department permutation
        ref = list(range(self.n_dep))
        self.assertTrue(all(e in ref for e in individual.fac))

        # Check department cut size
        self.assertEqual(len(individual.cut), self.n_dep)

        # Check validity of department cuts
        self.assertTrue(all(e == 0 or e == 1 for e in individual.cut))

    def test_fbs_representation(self):
        for _ in range(1000):
            indv = self.toolbox.individual()
            self.check_individual(indv)

    def test_population(self):
        test_sizes = [1, 2, 10, 20, 1000, 3000]
        test_sizes += random.sample(range(5000), 10)

        for test_size in test_sizes:
            population = self.toolbox.population(n=test_size)
            self.assertEqual(len(population), test_size)
            for ind in population: self.check_individual(ind)

    def test_mutation_valid(self):
        # Check that mutation doesn't put the individual in an invalid state
        for _ in range(100):
            indv = self.toolbox.individual()
            for _ in range(100):
                indv.mutate()
                self.check_individual(indv)

    def test_crossover_valid(self):
        # Check that crossover doesn't put the individuals in an invalid state
        for _ in range(100):
            indv1 = self.toolbox.individual()
            indv2 = self.toolbox.individual()
            for _ in range(100):
                indv1.crossover(indv2)
                self.check_individual(indv1)
                self.check_individual(indv2)

                # Swap the two to check that order doesn't matter
                indv1, indv2 = indv2, indv1


class EvaluationTestCase(UAFLPTestCase):
    def test_fitness_format(self):
        population = self.toolbox.population(n=1000)
        for indv in population:
            fit = self.toolbox.evaluate((indv.fac, indv.cut))
            self.assertIs(type(fit), tuple) # Fitness must be a tuple
            self.assertEqual(len(fit), 1) # That tuple must only contain one value
            self.assertGreaterEqual(fit[0], 0) # That value must be >= 0

    def test_penalty(self):
        for _ in range(100):
            penalty_evaluator = fbs.FlowWithAdaptativePenaltyEvaluator(self.problem)
            population = self.toolbox.population(n=100)
            for ind in population:
                ind.fitness.values = penalty_evaluator((ind.fac, ind.cut))

            for _ in range(100):
                newind = self.toolbox.individual()

                # Fitness without penalty
                costflow_newind = self.toolbox.evaluate((newind.fac, newind.cut))[0]
                # Fitness with penalty (used evaluator)
                cfpenalty_newind = penalty_evaluator((newind.fac, newind.cut))[0]

                # The fitness with penalty must be greater or equal
                self.assertGreaterEqual(cfpenalty_newind, costflow_newind)

class IslandTestCase(UAFLPTestCase):
    def test_communication_protocol(self):
        params = {
            'number_of_generations' : random.randint(50, 70),
            'number_of_generations_migrate' : random.randint(3, 7),
            'population_size_island' : random.randint(7,12),
            'mutation_prob' : random.uniform(0.05, 0.1),
            'crossover_prob' : random.uniform(0.6, 0.8),
        }
        island_id = 0
        self.toolbox.register('get_evaluator', fbs.FlowWithAdaptativePenaltyEvaluator, self.problem)
        qu = multiprocessing.SimpleQueue()
        recvPipe, sendPipe = multiprocessing.Pipe(duplex=False)
        island = WorkerIsland(island_id, params, qu, recvPipe, self.toolbox)

        # Launch island to test its communication
        process = multiprocessing.Process(target=island.run)
        process.start()
        migrants = 0
        while True:
            message = qu.get()
            self.assertEqual(message['sender'], island_id)
            self.assertIn(message['message'], ['migrant', 'finished'])
            if message['message'] == 'migrant':
                self.assertIs(type(message['migrant']), fbs.FBS)
                self.assertGreaterEqual(message['diversity'], 0)
                sendPipe.send({'message': 'no_migrant'})
                migrants += 1
            else:
                process.join()
                break

        # Check the correct number of generations were executed
        correct = int(math.ceil(params['number_of_generations'] / params['number_of_generations_migrate']))
        self.assertEqual(migrants, correct)

class SolutionsTestCase(unittest.TestCase):
    def test_correct_solutions(self):
        problem = fldata.Problem(filename='problems/test_problem.py')
        problem.max_distance = math.sqrt(13)*4
        parameters = fldata.Parameters(filename='test.params')
        alg = LocalizationStrategyAlgorithm()
        solutions = fldata.alg_main_params(alg, problem, parameters, 'test_sols')

        # Check that the parameters generated for each worker are bounded
        # by the input parameters' ranges
        original_params = parameters.parameters['workers_params_range']
        islands_params = parameters.parameters['workers_params']
        for island_params in islands_params:
            for key, value in island_params.items():
                lo, hi = original_params[key][0], original_params[key][1]
                self.assertTrue(lo <= value <= hi)

        # Check number of solutions
        max_n_solutions = parameters.parameters['n_workers'] * parameters.parameters['n_best_keep_per_island']
        self.assertLessEqual(len(solutions.solutions), max_n_solutions)

if __name__ == '__main__':
    unittest.main(verbosity=2)
