LATEXC = lualatex
LATEXFLAGS = -shell-escape -interaction=nonstopmode
BIBLATEXC = biber

CODFILES = $(find Code/*)
CHAPTERS = $(find Chapters/*.tex)
TABLES = $(find Tables/*.tex)

.POSIX:
.PHONY: clean cleanpdf cleanall manual_tecnico manual_usuario manual_codigo

all_digital: manual_tecnico.pdf manual_usuario.pdf manual_codigo.pdf
all_print: manual_tecnico_print.pdf manual_usuario_print.pdf manual_codigo_print.pdf
all: all_digital all_print
	make clean

manual_tecnico: manual_tecnico.pdf manual_tecnico_print.pdf
manual_codigo: manual_codigo.pdf manual_codigo_print.pdf
manual_usuario: manual_usuario.pdf manual_usuario_print.pdf

manual_tecnico.pdf: manual_tecnico.tex titlepage.tex preamble.tex $(CHAPTERS) $(TABLES) MastersDoctoralThesis.cls bibliografia.bib
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico" manual_tecnico.tex
	$(BIBLATEXC) manual_tecnico
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico" manual_tecnico.tex
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico" manual_tecnico.tex
	
manual_tecnico_print.pdf: manual_tecnico.tex titlepage.tex preamble.tex $(CHAPTERS) $(TABLES) MastersDoctoralThesis.cls bibliografia.bib
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico_print" "\AtBeginDocument{\printabledoc}\input{manual_tecnico.tex}"
	$(BIBLATEXC) manual_tecnico
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico_print" "\AtBeginDocument{\printabledoc}\input{manual_tecnico.tex}"
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_tecnico_print" "\AtBeginDocument{\printabledoc}\input{manual_tecnico.tex}"

manual_usuario.pdf: manual_usuario.tex titlepage.tex preamble.tex MastersDoctoralThesis.cls
	$(LATEXC) $(LATEXFLAGS) manual_usuario.tex
	$(LATEXC) $(LATEXFLAGS) manual_usuario.tex
	
manual_usuario_print.pdf: manual_tecnico.tex titlepage.tex preamble.tex $(CHAPTERS) $(TABLES) manual_tecnico.bbl MastersDoctoralThesis.cls
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_usuario_print" "\AtBeginDocument{\printabledoc}\input{manual_usuario.tex}"
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_usuario_print" "\AtBeginDocument{\printabledoc}\input{manual_usuario.tex}"
	
manual_codigo.pdf: manual_codigo.tex titlepage.tex preamble.tex $(CODEFILES) MastersDoctoralThesis.cls
	$(LATEXC) $(LATEXFLAGS) manual_codigo.tex
	$(LATEXC) $(LATEXFLAGS) manual_codigo.tex
	
manual_codigo_print.pdf: manual_tecnico.tex titlepage.tex preamble.tex $(CHAPTERS) $(TABLES) manual_tecnico.bbl MastersDoctoralThesis.cls
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_codigo_print" "\AtBeginDocument{\printabledoc}\input{manual_codigo.tex}"
	$(LATEXC) $(LATEXFLAGS) -jobname="manual_codigo_print" "\AtBeginDocument{\printabledoc}\input{manual_codigo.tex}"

clean: 
	rm -f *.aux *.bbl *.bcf *.blg *.lof *.log *.lot *.out *.run.xml *.tdo *.toc *.pyg  *.lol *.loa
	rm -f *.synctex.gz
	rm -f Figures/*-eps-converted-to.pdf
	rm -Rf _minted-*

cleanpdf:
	rm *.pdf

cleanall: clean cleanpdf
