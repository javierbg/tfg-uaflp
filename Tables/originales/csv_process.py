#!/usr/bin/env python3

import csv

filename_in = 'stats.csv'
filename_out = 'stats_p.csv'

fields_trim = [
	'fitness_mean',
	'diversity_mean',
	'cpu_time_mean',
]

with open(filename_in, 'r') as f_in:
	dr = csv.DictReader(f_in)
	fns = dr.fieldnames
	
	with open(filename_out, 'w') as f_out:
		dw = csv.DictWriter(f_out, fieldnames=fns)
		dw.writeheader()
		
		for row in dr:
			for field in fields_trim:
				row[field] = '{0:5.2f}'.format(float(row[field]))
				
			for field in row:
				row[field] = '$%s$' % row[field]

			dw.writerow(row)
