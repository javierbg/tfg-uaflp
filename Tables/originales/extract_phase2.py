import csv
from collections import defaultdict

import numpy as np


with open('phase2.csv', 'r') as file_in:
    reader = csv.DictReader(file_in)
    rows = [r for r in reader if r['repetition'] != 'average']

stat_names = [
    'mean_fitness',
    'diversity',
    'cpu_time',
    'number_of_solutions',
]

problems = {r['problem_name'] for r in rows}
repetitions = max({int(r['repetition']) for r in rows}) + 1
stats = dict()
for p in problems: stats[p] = {s: np.empty(repetitions) for s in stat_names}

for r in rows:
    for stat in stat_names:
        stats[r['problem_name']][stat][int(r['repetition'])] = r[stat]

fields_out = [
    'problem_name',
    'mean_fitness',
    'stdev(%)',
    'diversity',
    'mean_diversity',
]
with open('phase2sum.csv', 'w') as file_out:
    writer = csv.DictWriter(file_out, fieldnames=fields_out)
    writer.writeheader()
    for problem in problems:
        mean_fit = np.mean(stats[problem]['mean_fitness'])
        std_fit = np.std(stats[problem]['mean_fitness'])
        std_fit_rel = (std_fit / mean_fit)*100.0
        diversity = np.mean(stats[problem]['diversity'])
        n_solutions = np.mean(stats[problem]['number_of_solutions'])
        n_pairs = n_solutions * (n_solutions - 1) / 2
        mean_diversity = diversity / n_pairs
        writer.writerow({
            'problem_name': problem,
            'mean_fitness': mean_fit,
            'stdev(%)': std_fit_rel,
            'diversity': diversity,
            'mean_diversity': mean_diversity,
        })
